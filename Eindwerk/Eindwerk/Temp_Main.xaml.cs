﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using Eindwerk.View;

namespace Eindwerk
{
	public partial class Temp_Main : ContentPage
	{
		public Temp_Main ()
		{
			InitializeComponent ();
			this.Title = "ACHG Applicatie";
			//Bind menu items to command
			for (int i = 1; i <= 20; i++)
			{
				TextCell t = this.FindByName<TextCell> ("textCell"+i);
				if(t != null)
					t.Command = new Command (parameter => Open_Page(parameter));
			}
		}

		//Command to open page based on parameter
		async void Open_Page(object parameter)
		{/*
			int pageNumber = parameter == null ? -1 : Int32.Parse(parameter.ToString());
			if (pageNumber != -1)
			{
				Page page = null;
				//find page to switch to based on parameter
				switch (pageNumber)
				{
				case 1:
					page = new Parent_Glasgow_AVPU();
					break;
				case 2:
					page = new Oxigen();
					break;
				case 3:
					page = new ChestPain();
					break;
				case 4:
					page = new HartFailure();
					break;
				case 5:
					page = new Recovery_Position();
					break;
				case 6:
					page = new Unconscious_Parent();
					break;
				case 7:
					page = new TabbedSchemes();
					break;
				case 8:
					page = null;
					break;
				case 9:
					page = new Brain_Injury();
					break;
				case 10:
					page = new RuleOfNine();
					break;
				case 11:
					page = new Toxidrome();
					break;
				case 12:
					page = null;
					break;
				case 13:
					page = new Birth();
					break;
				case 15:
					page = new RithmPumpingAdults();
					break;
				case 16:
					page = new RithmPumpingChildren();
					break;
				case 17:
					page = null;
					break;
				case 18:
					page = new Medication_Reanimation();
					break;
				case 19:
					page = new Reanimation_Diagrams();
					break;
				case 20:
					page = new EarlyWarningSigns();

					break;
				case 21:
					page = new Main ();
					break;
				}
				//switch to page if found
				if (page != null)
				{
					await Navigation.PushModalAsync (page, false);
				}*/
			}
		}
	}
}