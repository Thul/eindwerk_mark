﻿using System;
using System.Linq;
using System.Collections.Generic;

using Xamarin.Forms;
using Eindwerk.ViewModels;
using Eindwerk.Business.Logic.Service;

namespace Eindwerk.View.Menu
{
	public partial class Main : ContentPage
	{
		private MainViewModel _viewModel;
		private Grid _primaryGrid;
		private GridLayoutManager _layoutManager;
		private Dictionary<string, StackLayout> _popupMenus;
		private SearchBar _searchBar;
		private IEnumerable<ArticleViewModel> _articles;

		public Main ()
		{
			InitializeComponent ();
			_searchBar = this.FindByName<SearchBar> ("searchBar");
			_searchBar.SearchCommand = new Command (() => { SearchArticlesClicked(_searchBar.Text); });
			_viewModel = new MainViewModel ();
			_articles =  _viewModel.GetArticles ();
			_primaryGrid = this.FindByName<Grid> ("primaryMenuItemsGrid");
			_layoutManager = new GridLayoutManager (this); //handles resizing dynamic grids based on screen dimensions
			_popupMenus = new Dictionary<string, StackLayout> ();
			CreatePrimaryTiles (_viewModel.GetCategoryViewModels());
			this.SizeChanged += (object sender, EventArgs e) => _layoutManager.ReloadDimensionsAndGrids ();
			_layoutManager.ReloadDimensionsAndGrids ();
		}

		//Creates tiles on main menu, using given list of CategoryViewModels
		private void CreatePrimaryTiles (IEnumerable<CategoryViewModel> primaryMenuItems)
		{
			//create main menu
			List<Grid> tiles = new List<Grid> ();
            foreach (CategoryViewModel primaryMenuItem in primaryMenuItems) {
				Grid tile = CreatePrimaryTile (primaryMenuItem, 70, 18);
				tiles.Add (tile);
			}
			//create dynamic grid with tiles from above and add it to layoutmanager so its dimensions get recalculated when screen size changes
			DynamicGrid primaryDynamicGrid = new DynamicGrid (_primaryGrid, tiles, 130);
			_layoutManager.AddGrid (primaryDynamicGrid);
			_layoutManager.ReloadDimensionsAndGrids ();
		}

		//Creates and returns a primary tile (primary = on main menu)
		private Grid CreatePrimaryTile (CategoryViewModel categoryVM, int imageSize, int fontSize)
		{
			//Tile
			Grid tile = new Grid () {
				BackgroundColor = Color.FromHex(categoryVM.Color) //remove this if using frame
			};
			//Layout
			StackLayout layout = new StackLayout { //can be replaced with Grid, RelativeLayout, etc.
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center,
			};
			/*Frame layoutContainer = new Frame {
				Content = layout,
				Padding = 5,
				BackgroundColor = Color.FromHex(categoryVM.Color)
			};*/ //add frame instead of layout to tile if you want border radius on tile
			//Content
			Image image = new Image {
				WidthRequest = imageSize,
				HeightRequest = imageSize,
				Source = ImageSource.FromFile (categoryVM.ImagePath),
			};
			layout.Children.Add (image);
			Label label = new Label {
				TextColor = Color.White,
				FontSize = fontSize,
				Text = categoryVM.Name,
				HorizontalTextAlignment = TextAlignment.Center,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center
			};
			layout.Children.Add (label);
			//Invisible button
			Button button = new Button {
				TextColor = Color.Transparent,
				BackgroundColor = Color.Transparent,
				BorderColor = Color.Transparent
			};
			//Action to be undertaken upon icon tap
			if (categoryVM.Categories.Count() == 0 && categoryVM.Articles.Count() == 1) { //no categories and only one article -> no popup
				button.Command = new Command (parameter => OpenPage_Command (parameter));
				button.CommandParameter = categoryVM.Articles.ElementAt(0).Id;
			} else { //several categories and/or several articles -> submenu for given item and for all of its children categories
				CreatePopupMenu (categoryVM, button);
                foreach(CategoryViewModel subCategory in categoryVM.Categories) CreatePopupMenu(subCategory);
			}
			//Add container and button to tile
			tile.Children.Add (layout);
			tile.Children.Add (button); //idea: revert order if it doesn't work on iphone
			//Return tile
			return tile;
		}

		/// <summary> Searches for all related articles, on tag as well as on title. The result
		/// is contained within a new categoryviewmodel and given to the CreatePopupMenu method 
		/// </summary>
		private void SearchArticlesClicked(string keyword) {
			// Remove previous search pop-up menu if present
			if (_popupMenus.ContainsKey("Zoekresultaten")) {
				this.FindByName<Grid> ("subMenuLayoutsContainer").Children.Remove (_popupMenus["Zoekresultaten"]);
				_popupMenus.Remove("Zoekresultaten");
			}
			// Get a list of all articles for which the title contains the given keyword
			IEnumerable<ArticleViewModel> articlesTitle = _articles.Where((x) => x.Title.Contains(keyword));
			// Get a list of all articles for which the keyword contains one of the tags
			IEnumerable<ArticleViewModel> articlesTag = _articles.Where((x)=>x.Tags.Where((y)=> keyword.Contains(y.Name)).Count() > 0);
			// Join articles
			IEnumerable<ArticleViewModel> searchResultArticles = articlesTitle.Union(articlesTag);
			// Create CategoryViewModel with the articles to pass to CreatePopupMenu() method
			CategoryViewModel resultCategory = new CategoryViewModel { 
				Articles = searchResultArticles,
				Name = "Zoekresultaten",
				Color = "#0095FF",
				Categories = new List<CategoryViewModel>()
			};
			// Create pop-up and save it under variable
			CreatePopupMenu (resultCategory);
			// Show pop-up
			ShowPopup_Command (resultCategory.Name);
		}

		//Creates a popup menu based on a given parent CategoryViewModel (which contains a list of articles), with an optional reference to an existing main menu button which serves as the trigger for the popup
		private void CreatePopupMenu (CategoryViewModel parentCategoryVM, Button mainMenuTrigger = null)
		{
			Grid subMenuLayoutsContainer = this.FindByName<Grid> ("subMenuLayoutsContainer");
			//create new sub menu layout
			StackLayout subMenuLayout = new StackLayout () { BackgroundColor = Color.White, IsVisible = false };
			ContentView subMenuTitle = new ContentView () {
				Content = new Label () {
					Text = parentCategoryVM.Name,
					FontSize = 20,
					TextColor = Color.FromHex(parentCategoryVM.Color),
					HorizontalTextAlignment = TextAlignment.Center
				},
				Padding = new Thickness (0, 10, 0, 5)
			};
			subMenuLayout.Children.Add (subMenuTitle);
			Grid subMenuTargetGrid = new Grid () { ColumnSpacing = 1, RowSpacing = 15, Padding=5 };
			subMenuLayout.Children.Add (subMenuTargetGrid);
			subMenuLayout.Children.Add (new Label { Text="" }); //automatic padding after items grid
			List<Grid> subTiles = new List<Grid> ();
			foreach (CategoryViewModel categoryItem in parentCategoryVM.Categories) {
				Grid subTile = CreateSubCategoryTile (categoryItem, 60);
				subTiles.Add (subTile);
			}
			foreach (ArticleViewModel articleItem in parentCategoryVM.Articles) {
				Grid subTile = CreateArticleTile (articleItem, 60);
				subTiles.Add (subTile);
			}
			DynamicGrid subMenuGrid = new DynamicGrid (subMenuTargetGrid, subTiles, 100);
			_layoutManager.AddGrid (subMenuGrid);
			subMenuLayoutsContainer.Children.Add (subMenuLayout);
			_popupMenus.Add (parentCategoryVM.Name, subMenuLayout);
            if (mainMenuTrigger != null) //if main menu item was given, set its command to open the newly created sub/popup menu
            {
                mainMenuTrigger.Command = new Command(parameter => ShowPopup_Command(parameter));
                mainMenuTrigger.CommandParameter = parentCategoryVM.Name;
            }
		}

		//Creates and returns sub tile (sub tile = on sub/popup menu)
		//For category tiles (which can open another sub/popup menu
		private Grid CreateSubCategoryTile (CategoryViewModel categoryVM, int imageSize)
		{
			Command showPopupCommand = new Command (parameter => ShowPopup_Command (parameter));
			string commandParameter = categoryVM.Name;
			return CreateSubTile (showPopupCommand, commandParameter, categoryVM.Name, categoryVM.ImagePath, imageSize, categoryVM.Color);
		}
		//For article tiles (which are direct links to pages)
		private Grid CreateArticleTile (ArticleViewModel articleVM, int imageSize)
		{
			Command openPageCommand = new Command (parameter => OpenPage_Command (parameter));
			string commandParameter = articleVM.Id + ""; //convert to string
			return CreateSubTile (openPageCommand, commandParameter, articleVM.Title, articleVM.Image, imageSize, articleVM.Color);
		}
		//Used to create category or article tile as part of a sub menu (serves both methods above, which differ only in command)
		private Grid CreateSubTile(Command command, string commandParameter, string title, string imagePath, int imageSize, string backgroundColor,  int id = -1)
		{
			//Layout
			Grid tile = new Grid ();
			//Content
			StackLayout container = new StackLayout {
				Padding = new Thickness (5)
			};
			Image image = new Image {
				WidthRequest = imageSize,
				HeightRequest = imageSize,
				Source = ImageSource.FromFile (imagePath)
			};
			Frame imageContainer = new Frame {
				Content = image,
				Padding = 0,
				BackgroundColor = Color.FromHex(backgroundColor),
				HorizontalOptions = LayoutOptions.Center
			};
			container.Children.Add (imageContainer);
			Label label = new Label {
				TextColor = Color.Black,
				FontSize = 15,
				Text = title,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center,
				HorizontalTextAlignment = TextAlignment.Center
			};
			container.Children.Add (label);
			//Invisible button
			Button button = new Button {
				Command = command,
				CommandParameter = commandParameter,
				TextColor = Color.Transparent,
				BackgroundColor = Color.Transparent,
				BorderColor = Color.Transparent
			};
			//Add container and button to tile
			tile.Children.Add (container);
			tile.Children.Add (button);
			//return tile
			return tile;
		}

		//Opens page based on given ID
		async void OpenPage_Command (object parameter)
		{
			if (parameter != null) {
				int pageId = Int32.Parse (parameter.ToString ());
				Page page = ServiceLocator.GetInstance ().GetNavigationService ().NavigateTo (pageId);
				if (page != null) {
					await this.Navigation.PushAsync (page);
				}
			}
		}

		//Shows appropriate popup based on given title
		void ShowPopup_Command (object parameter)
		{
			_popupMenus [parameter.ToString ()].IsVisible = true;
			this.FindByName<Grid> ("popupLayout").IsVisible = true;
			this.FindByName<StackLayout> ("dimBackground").IsVisible = true;
			foreach (string subMenuKey in _popupMenus.Keys) {
				if (subMenuKey != parameter.ToString ()) {
					_popupMenus [subMenuKey].IsVisible = false;
				}
			}
		}

		//Hides popup container and hides background dim layer
		void Hide_Popups_Clicked (object sender, EventArgs e)
		{
			this.FindByName<Grid> ("popupLayout").IsVisible = false;
			this.FindByName<StackLayout> ("dimBackground").IsVisible = false;
		}

		//Controls what happens when back button is pressed (if popup menu is open -> close menu but not app; else close app)
		protected override bool OnBackButtonPressed ()
		{
			Hide_Popups_Clicked (this, null);
			return true;
		}
	}
}