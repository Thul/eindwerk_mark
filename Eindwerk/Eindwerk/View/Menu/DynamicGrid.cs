﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;

namespace Eindwerk.View.Menu
{
	public class DynamicGrid
	{
		private Grid _targetGrid;
		private int _tileSize;
		private List<Grid> _menuTiles;

		public List<Grid> MenuTiles {
			get { return _menuTiles; }
			set { _menuTiles = value; }
		}

		public DynamicGrid (Grid targetGrid, List<Grid> menuTiles = null, int tileSize = 60)
		{
			_targetGrid = targetGrid;
			_tileSize = tileSize;
			_menuTiles = (menuTiles != null ? menuTiles : new List<Grid> ());
		}

		public void ReloadTiles ()
		{
			//determine screen dimensions
			double widthScreen = App.ScreenSize.Width;
			double heightScreen = App.ScreenSize.Height;
			//using screen dimensions, determine a) number of columns based on tile size and b) number of rows based on number of tiles
			int cols = (int)Math.Floor ((double)((widthScreen) / _tileSize));
			int rows = (int)Math.Ceiling ((double)_menuTiles.Count / cols);
			//a) remove menu items, b) set up new column and row definitions and c) add menu items again
			_targetGrid.Children.Clear ();
			_targetGrid.RowDefinitions = new RowDefinitionCollection ();
			_targetGrid.ColumnDefinitions = new ColumnDefinitionCollection ();
			for (int i = 0; i != cols; ++i) {
				_targetGrid.ColumnDefinitions.Add (new ColumnDefinition{ Width = new GridLength (1, GridUnitType.Star) });
			}
			int tilesRemaining = _menuTiles.Count;
			for (int i = 0; i != rows; ++i) {
				_targetGrid.RowDefinitions.Add (new RowDefinition { Height = new GridLength (_tileSize - _targetGrid.ColumnSpacing, GridUnitType.Absolute) });
				for (int j = 0; j != cols; ++j) {
					if (tilesRemaining > 0) {
						_targetGrid.Children.Add (_menuTiles [tilesRemaining-1], j, i);
						tilesRemaining--;
					}
				}
			}
		}
	}
}

