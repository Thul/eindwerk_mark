﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;

namespace Eindwerk.View.Menu
{
	public class GridLayoutManager
	{
		//references
		private Page _targetPage;
		private List<DynamicGrid> _grids;
		//properties
		private double _previousWidth;
		private string _previousOrientation;

		public GridLayoutManager (Page targetPage, List<DynamicGrid> grids = null)
		{
			_targetPage = targetPage;
			_grids = (grids != null ? grids : new List<DynamicGrid> ());
			_previousWidth = 0;
			_previousOrientation = "";
			ReloadGrids ();
		}

		public void AddGrid(DynamicGrid grid)
		{
			_grids.Add(grid);
			grid.ReloadTiles();
		}

		//Requests each dynamic grid to reload itself
		private void ReloadGrids ()
		{
			foreach (DynamicGrid grid in _grids) {
				grid.ReloadTiles ();
			}
		}

		//Configures new screen settings and calls ReloadGrids()
		public void ReloadDimensionsAndGrids ()
		{
			//Determine user's "environment" (screen width, height, orientation, ...)
			double widthScreen = App.ScreenSize.Width;
			double heightScreen = App.ScreenSize.Height;
			double smallest = widthScreen < heightScreen ? widthScreen : heightScreen;
			double largest = smallest == widthScreen ? heightScreen : widthScreen;
			//Compare new width with previous width, if same then the keyboard has opened or closed, and so grid layout must NOT be refreshed 
			if (_targetPage.Width == _previousWidth) {// = no grid resizing
				//Save description of new orientation to a variable
				_previousOrientation = (_previousOrientation == "portraitKeyboard" ? "landscapeKeyboard" : "portraitKeyboard");
			} else { // = grid resizing
				if (_targetPage.Width < _targetPage.Height) {
					App.ScreenSize = new Size (smallest, largest);
				} else {
					if (_previousOrientation == "landscape") {
						App.ScreenSize = new Size (smallest, largest);
					} else {
						App.ScreenSize = new Size (largest, smallest);
					}
				}
				//Refresh grids
				ReloadGrids();
				//Save new orientation and width to variables
				_previousOrientation = App.ScreenSize.Width < App.ScreenSize.Height ? "portrait" : "landscape";
				_previousWidth = _targetPage.Width;
			}
		}

	}
}

