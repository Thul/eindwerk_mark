﻿using System;
using System.Collections.Generic;
using Eindwerk.ViewModels;
using Xamarin.Forms;



namespace Eindwerk.View.Pages.Monitoring
{
	public partial class ZuurstofToediening : ContentPage
	{
		private MainViewModel _mainViewModel;

		public ZuurstofToediening (MainViewModel vm)
		{
			_mainViewModel = vm;
			this.BindingContext = _mainViewModel;
			InitializeComponent ();
		}

		public ZuurstofToediening (){
			InitializeComponent ();
		}

	}
}

