﻿using System;
using System.Collections.Generic;
using Eindwerk.ViewModels;
using Xamarin.Forms;

namespace Eindwerk.View.Pages.Monitoring
{
	public partial class Hartfalen : ContentPage
	{
		private MainViewModel _mainViewModel;
		public Hartfalen (MainViewModel viewModel = null)
		{
			if (viewModel != null) {
				_mainViewModel = viewModel;
				this.BindingContext = _mainViewModel;
			}
			InitializeComponent ();
        }
	}
}

