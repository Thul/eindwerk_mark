﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace Eindwerk.View.Pages.Monitoring
{
	public partial class Glasgow : ContentPage
	{

		private Label _resultLabel;
		private Button _eyesPicker, _motorPicker, _verbalPicker;
		private int _eyesScore, _motorScore, _verbalScore;

		public Glasgow ()
		{
			InitializeComponent ();
			_resultLabel = this.FindByName<Label> ("lblResult");
			_eyesPicker = this.FindByName<Button> ("pkrEyes");
			_motorPicker = this.FindByName<Button> ("pkrMotor");
			_verbalPicker = this.FindByName<Button> ("pkrVerbal");
			_eyesScore = 0; //
			_motorScore = 0; // unnecessary but explicit and consistent
			_verbalScore = 0; //
		}

		async void pkrEyes_Clicked(object sender, EventArgs e)
		{
			string result = await DisplayActionSheet ("Ogen reactie:", "Annuleren", null, "1 - Niet", "2 - Op pijnprikkels", "3 - Op geluid", "4 - Spontaan");
			if (result != null && result != "Annuleren") {
				Int32.TryParse(result.Substring (0, 1), out _eyesScore);
				((Button)sender).Text = result;
				ShowResult ();
			}
		}

		async void pkrMotor_Clicked(object sender, EventArgs e)
		{
			string result = await DisplayActionSheet ("Motore reactie:", "Annuleren", null, "1 - Geen reactie", "2 - Strekt", "3 - Buigt pathologisch", "4 - Buigt normaal", "5 - Lokaliseert", "6 - Voert opdrachten uit");
			if (result != null && result != "Annuleren") {
				Int32.TryParse(result.Substring (0, 1), out _motorScore);
				((Button)sender).Text = result;
				ShowResult ();
			}
		}

		async void pkrVerbal_Clicked(object sender, EventArgs e)
		{
			string result = await DisplayActionSheet ("Verbale reactie:", "Annuleren", null, "1 - Geen geluid", "2 - Geluiden, geen woorden", "3 - Een enkel woord", "4 - Verward", "5 - Georiënteerd");
			if (result != null && result != "Annuleren") {
				Int32.TryParse(result.Substring (0, 1), out _verbalScore);
				((Button)sender).Text = result;
				ShowResult ();
			}
		}

		private void ShowResult ()
		{
			if (_eyesScore != 0 && _motorScore != 0 && _verbalScore != 0)
			{
				int result = _eyesScore + _motorScore + _verbalScore;
				string description = "";
				if (result == 15) {
					description = "beste respons";
					_resultLabel.TextColor = Color.Green;
				} else if (result >= 13) {
					description = "licht";
					_resultLabel.TextColor = Color.FromHex("#FFA500");
				} else if (result >= 9) {
					description = "matig";
					_resultLabel.TextColor = Color.Maroon;
				} else {
					description = "ernstig";
					_resultLabel.TextColor = Color.Red;
				}
				_resultLabel.Text = "Resultaat: "+result+"/15 - "+description;
			}
		}

	}
}
