﻿using System;
using System.Collections.Generic;
using Eindwerk.ViewModels;
using Xamarin.Forms;

namespace Eindwerk.View.Pages.Monitoring
{
	public partial class ElectrodesPicco : ContentPage
	{
		private MainViewModel _mainViewModel;
		public ElectrodesPicco (MainViewModel vm)
		{
            _mainViewModel = vm;
            this.BindingContext = vm;
			InitializeComponent ();
		}
	}
}

