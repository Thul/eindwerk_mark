﻿using System;
using System.Collections.Generic;
using Eindwerk.ViewModels;
using Xamarin.Forms;
using System.Linq;
using Java.Lang;
using System.Threading.Tasks;

namespace Eindwerk.View.Pages.Monitoring
{
	public partial class EarlyWarningSigns : ContentPage
	{
		private Label _Result,  _lblHeading2, _lblHeading3;
		//private Picker _AvpuPicker;
		private Button _avpuPicker;
		private int _avpuPickerResult;
		private Grid _gridMeting1,_gridMeting2, _gridMeting3;
		private Button _btnNextColumn;
		private int activeColumns = 1;
		private int[] resultColour = { 0, 0, 0 };

		private MainViewModel _mainViewModel;

		public EarlyWarningSigns (MainViewModel vm = null)
		{
			if (vm != null) {
				_mainViewModel = vm;
				this.BindingContext = vm;
			}
			InitializeComponent ();

			//_AvpuPicker = this.FindByName<Picker> ("pkrAvpu"+activeColumns);
			_avpuPicker = this.FindByName<Button> ("pkrAvpu1");
			_avpuPickerResult = -1;

			_btnNextColumn = this.FindByName<Button> ("btnNextColumn");
			_Result = this.FindByName<Label> ("lblResult1");

			_gridMeting1 = this.FindByName<Grid> ("gridMeting1");

			_gridMeting2 = this.FindByName<Grid> ("gridMeting2");
			_gridMeting2.IsVisible = false;

			_gridMeting3 = this.FindByName<Grid> ("gridMeting3");
			_gridMeting3.IsVisible = false;

			_lblHeading2 = this.FindByName<Label> ("lblHeading2");
			_lblHeading2.IsVisible = false;

			_lblHeading3 = this.FindByName<Label> ("lblHeading3");
			_lblHeading3.IsVisible = false;

		}

		async void Pkr_Clicked(object sender, EventArgs e)
		{
			string result = await DisplayActionSheet ("AVPU", "Annuleren", null, "A - Alert", "V - Reageert op stem", "P - Reageert op pijn of is verward", "U - Reageert niet");
			if (result != null) {
				string avpu = result.Substring (0, 4); //letter A, V, P or U
				if (avpu == "A - ") { //" - " is to avoid "Annuleren" being counted as the "A" from AVPU; done everywhere to be safe from whatever language phone is in (where Cancel might be one of the other AVPU letters)
					_avpuPickerResult = 0;
				} else if (avpu == "V - ") {
					_avpuPickerResult = 1;
				} else if (avpu == "P - ") {
					_avpuPickerResult = 2;
				} else if (avpu == "U - ") {
					_avpuPickerResult = 3;
				} else {
					return;
				}
				_avpuPicker.Text = avpu.Substring(0, 1);
				ShowResult ();
			}
		}

		//<---- Respiration Entry ---->

		private void Respiration1_Changed(object sender, TextChangedEventArgs e) 
		{
				ShowResult ();
		}

		private int CalculateRespiration(double value)
		{
			if (value < 8) {
				return 3;
			} else if (value <= 20) {
				return 0;
			} else if (value <= 30) {
				return 1;
			} else {
				return 3; 
			}
		}

		//<---- Heart Entry ---->

		private void HeartEntry1_Changed(object sender, TextChangedEventArgs e)
		{
				ShowResult ();
		}

	
		private int CalculateHeart(double value) {
			if (value < 40) {
				return 3;
			} else if (value <= 50) {
				return 1;
			} else if (value <= 100) {
				return 0;
			} else if (value <= 110) {
				return 1;
			} else if (value <= 130) {
				return 2;
			} else {
				return 3; 
			}
		}

		//<---- TaSystolique Entry ---->

		private void TaSystoliqueEntry1_Changed(object sender, TextChangedEventArgs e)
		{
				ShowResult ();
		}


		private int CalculateSystolique(double value) {
			if (value < 70) {
				return 3;
			} else if (value <= 80) {
				return 2;
			} else if (value <= 100) {
				return 1;
			} else if (value <= 180) {
				return 0;
			} else if (value <= 200) {
				return 1;
			} else if (value <= 200) {
				return 2;
			} else {
				return 3; 
			}
		}

		//<---- Temperature Entry ---->

		private void TemperatureEntry1_Changed(object sender, TextChangedEventArgs e)
		{
				ShowResult ();
		}


		private int CalculateTemperature(double value) {
			if (value < 34) {
				return 3;
			} else if (value <= 35) {
				return 2;
			} else if (value <= 37.5) {
				return 0;
			} else if (value <= 38.5) {
				return 1;
			} else if (value <= 40) {
				return 2;
			} else {
				return 3; 
			}
		}

		//<---- SpO² Entry ---->

		private void SpoEntry1_Changed(object sender, TextChangedEventArgs e)
		{
				ShowResult ();

		}


		private int CalculateSpo(double value) {
			if (value <= 90) {
				return 3;
			} else if (value <= 93) {
				return 2;
			} else {
				return 0; 
			}
		}

		//<---- AVPU Entry ---->

		private void AvpuEntry1_SelectedIndexChanged(object sender, EventArgs e)
		{
			ShowResult ();
		}


		private int CalculateAvpu(int index) {
				switch (index) {
					case 0:
						return 0;
					case 1:
						return 1;
					case 2:
						return 2;
					case 3: 
						return 3;
				} 
			return 0;
		} 
			
		//<---- Show Result ---->

		private void ShowResult()
		{
		
			int score = 0;
			double n;

			Entry respirationEntry = this.FindByName<Entry> ("RespirationEntry"+activeColumns);
			if (respirationEntry.Text != "" && double.TryParse (respirationEntry.Text, out n)) {
				int scoreRespiration = CalculateRespiration (double.Parse (respirationEntry.Text)); 
				if (scoreRespiration == 0) {
					respirationEntry.TextColor = Color.Green;
					resultColour [0] = 1;
				} else if (scoreRespiration <= 2) {
					respirationEntry.TextColor = Color.FromHex ("#FF9900");
					resultColour [1] = 1;
				} else { 
					respirationEntry.TextColor = Color.Red;
					resultColour [2] = 1;
				}
				score += scoreRespiration;	
			}
		
			Entry hearthEntry = this.FindByName<Entry> ("HeartEntry"+activeColumns);
			if (hearthEntry.Text != "" && double.TryParse (hearthEntry.Text, out n)) {
				int scoreHeart = CalculateHeart (double.Parse (hearthEntry.Text));
				if (scoreHeart == 0) {
					hearthEntry.TextColor = Color.Green;
					resultColour [0] = 1;
				} else if (scoreHeart <= 2) {
					hearthEntry.TextColor = Color.FromHex ("#FF9900");
					resultColour [1] = 1;
				} else { 
					hearthEntry.TextColor = Color.Red;
					resultColour [2] = 1;
				}
				score += scoreHeart;
			}

			Entry systoliqueEntry = this.FindByName<Entry> ("TaSystoliqueEntry"+activeColumns);
			if (systoliqueEntry.Text != "" && double.TryParse (systoliqueEntry.Text, out n)) {
				int scoreSystolique = CalculateSystolique (double.Parse (systoliqueEntry.Text));
				if (scoreSystolique == 0) {
					systoliqueEntry.TextColor = Color.Green;
					resultColour [0] = 1;
				} else if (scoreSystolique <= 2) {
					systoliqueEntry.TextColor = Color.FromHex ("#FF9900");
					resultColour [1] = 1;
				} else { 
					systoliqueEntry.TextColor = Color.Red;
					resultColour [2] = 1;
				}
				score += scoreSystolique;
			}


			Entry temperatureEntry = this.FindByName<Entry> ("TemperatureEntry"+activeColumns);
			if (temperatureEntry.Text != "" && double.TryParse (temperatureEntry.Text, out n)) {
				int scoreTemperature = CalculateTemperature (double.Parse (temperatureEntry.Text));
				if (scoreTemperature == 0) {
					temperatureEntry.TextColor = Color.Green;
					resultColour [0] = 1;
				} else if (scoreTemperature <= 2) {
					temperatureEntry.TextColor = Color.FromHex ("#FF9900");
					resultColour [1] = 1;
				} else { 
					temperatureEntry.TextColor = Color.Red;
					resultColour [2] = 1;
				}
				score += scoreTemperature;
			}

			Entry spoEntry = this.FindByName<Entry> ("SpoEntry"+activeColumns);
			if (spoEntry.Text != "" && double.TryParse (spoEntry.Text, out n)) {
				int scoreSpo = CalculateSpo (double.Parse (spoEntry.Text));
				if (scoreSpo == 0) {
					spoEntry.TextColor = Color.Green;
					resultColour [0] = 1;
				} else if (scoreSpo <= 2) {
					spoEntry.TextColor = Color.FromHex ("#FF9900");
					resultColour [1] = 1;
				} else { 
					spoEntry.TextColor = Color.Red;
					resultColour [2] = 1;
				}
				score += scoreSpo;
			}

			//_AvpuPicker = this.FindByName<Picker> ("pkrAvpu"+activeColumns);
			if (_avpuPickerResult != -1) {
				int scoreAvpu = CalculateAvpu (_avpuPickerResult);
				if (scoreAvpu == 0) {
					resultColour [0] = 1;
				} else if (scoreAvpu <= 2) {
					resultColour [1] = 1;
				} else {
					resultColour [2] = 1;
				}

				score += scoreAvpu;
			}

			_Result.Text = score.ToString ();

			if (resultColour [2] == 1) {
				_Result.TextColor = Color.Red;
			} else if (resultColour [1] == 1) {
				_Result.TextColor = Color.FromHex ("#FF9900");
			} else {
				_Result.TextColor = Color.Green;
			}
			resultColour = new int[]{ 0, 0, 0 };


		}
			

		private void ShowNextCalculationColumns(object sender, EventArgs args) {
			resultColour = new int[]{ 0,0,0 };

			if (activeColumns == 1)
			{ //show Second calculation
				activeColumns++;

				_lblHeading2.IsVisible = true;
				_gridMeting2.IsVisible = true;
				_Result = this.FindByName<Label> ("lblResult2");
				_Result.Text = "0";
				_avpuPicker.IsEnabled = false;
				_avpuPicker = this.FindByName<Button> ("pkrAvpu2");
				_avpuPickerResult = -1;
			}
			else
			{ //show Last calculation
				activeColumns++;

				_lblHeading3.IsVisible = true;
				_gridMeting3.IsVisible = true;
				_btnNextColumn.IsVisible = false;
				_btnNextColumn.IsEnabled = false;
				_Result = this.FindByName<Label> ("lblResult3");
				_Result.Text = "0";
				_avpuPicker.IsEnabled = false;
				_avpuPicker = this.FindByName<Button> ("pkrAvpu3");
				_avpuPickerResult = -1;
			}
		}
			
	}
}


