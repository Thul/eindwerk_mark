﻿using System;
using Eindwerk.ViewModels;
using Xamarin.Forms;

namespace Eindwerk.View.Pages.Monitoring
{
	public class Bewustzijnsverlies : TabbedPage
	{
		private MainViewModel _mainViewModel;
		public Bewustzijnsverlies (MainViewModel vm = null)
		{
			if (vm != null) {
				_mainViewModel = vm;
				this.BindingContext = _mainViewModel;
				this.Title = _mainViewModel.Article.Title;
			}
			this.Children.Add(new BewustzijnsverliesPlots());
			this.Children.Add(new BewustzijnsverliesProgr());
		}
	}
}


