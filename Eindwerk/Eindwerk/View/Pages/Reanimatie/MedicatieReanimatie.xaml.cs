﻿using System;
using System.Collections.Generic;
using Eindwerk.ViewModels;
using Xamarin.Forms;

namespace Eindwerk.View.Pages.Reanimatie
{
	public partial class MedicatieReanimatie : ContentPage
	{
		private MainViewModel _mainViewModel;
		public MedicatieReanimatie (MainViewModel vm = null)
		{
			if (vm != null) {
				_mainViewModel = vm;
				this.BindingContext = _mainViewModel;
			}
			InitializeComponent ();
		}
	}
}