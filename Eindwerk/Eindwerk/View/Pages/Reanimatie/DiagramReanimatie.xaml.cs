﻿using System;
using System.Collections.Generic;
using Eindwerk.ViewModels;
using Xamarin.Forms;

namespace Eindwerk.View.Pages.Reanimatie
{
	public partial class DiagramReanimatie : ContentPage
	{
		//scroll view
		private ScrollView _scrollView;
		//primary titles
		private Label _lblControleerBewustzijn, _lblIndienGeenReactie, _lblGeenNormaleAdemhaling;
		//primary content
		private StackLayout _stckControleerBewustzijn, _stckIndienGeenReactie, _stckGeenNormaleAdemhaling;
		private MainViewModel _mainViewModel;

		public DiagramReanimatie (MainViewModel vm = null)
		{
			if (vm != null) {
				_mainViewModel = vm;
				this.BindingContext = _mainViewModel;
			}
			//initialise xaml etc
			InitializeComponent ();
			//link instance variables to controls
			_scrollView = this.FindByName<ScrollView> ("scrollView");
			//primary titles
			_lblControleerBewustzijn = this.FindByName<Label> ("lblControleerBewustzijn");
			_lblIndienGeenReactie = this.FindByName<Label> ("lblIndienGeenReactie");
			_lblGeenNormaleAdemhaling = this.FindByName<Label> ("lblGeenNormaleAdemhaling");
			//primary content
			_stckControleerBewustzijn = this.FindByName<StackLayout> ("stckControleerBewustzijn");
			_stckIndienGeenReactie = this.FindByName<StackLayout> ("stckIndienGeenReactie");
			_stckGeenNormaleAdemhaling = this.FindByName<StackLayout> ("stckGeenNormaleAdemhaling");
			//hide content (done here because, if done directly in xaml, dimensions change when isVisible goes to 'false' for the first time)
			_stckControleerBewustzijn.IsVisible = true;
			_stckIndienGeenReactie.IsVisible = true;
			_stckGeenNormaleAdemhaling.IsVisible = true;
		}

		void ControleerBewustzijn_Clicked(object sender, EventArgs args)
		{
			if (_lblControleerBewustzijn != null && _stckControleerBewustzijn != null)
			{
				Show_Or_Hide_Content(_lblControleerBewustzijn, _stckControleerBewustzijn);
			}
		}

		void IndienGeenReactie_Clicked(object sender, EventArgs args)
		{
			if (_lblIndienGeenReactie != null && _stckIndienGeenReactie != null)
			{
				Show_Or_Hide_Content (_lblIndienGeenReactie, _stckIndienGeenReactie);
			}
		}

		void GeenNormaleAdemhaling_Clicked(object sender, EventArgs args)
		{
			if (_lblGeenNormaleAdemhaling != null && _stckGeenNormaleAdemhaling != null)
			{
				Show_Or_Hide_Content (_lblGeenNormaleAdemhaling, _stckGeenNormaleAdemhaling);
			}
		}
			
		void Show_Or_Hide_Content(Label titleLabel, StackLayout stackLayout)
		{
			if (!stackLayout.IsVisible)
			{ //show content
				stackLayout.IsVisible = true;
				titleLabel.Text = "v" + titleLabel.Text.Substring(1);
				_scrollView.ScrollToAsync (0, stackLayout.Y - ((ContentView)titleLabel.Parent).Height, true);
			}
			else
			{ //hide content
				stackLayout.IsVisible = false;
				titleLabel.Text = ">" + titleLabel.Text.Substring(1);
			}
		}

		void Reanimeren_Clicked(object sender, EventArgs args) {
			Navigation.PushAsync (new HartmassageVolwassenen (_mainViewModel));
		}
	}
}

