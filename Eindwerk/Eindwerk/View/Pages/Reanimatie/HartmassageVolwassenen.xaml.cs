﻿using System;
using System.Collections.Generic;

using Eindwerk.ViewModels;
using Plugin.Messaging;
using Xamarin.Forms;
using Eindwerk.Business.Logic;

namespace Eindwerk.View.Pages.Reanimatie
{
	public partial class HartmassageVolwassenen : ContentPage
	{
		private MainViewModel _mainViewModel;
		public HartmassageVolwassenen (MainViewModel vm = null)
		{
			if (vm != null) {
				_mainViewModel = vm;
				this.BindingContext = _mainViewModel;
			}
			InitializeComponent ();
		}

		void PlayButton_Clicked(object sender, EventArgs args)
		{
			DependencyService.Get<IAudio>().PlayMp3File(
				"adult.mp3"
			);
		}
		void StopButton_Clicked(object sender, EventArgs args)
		{
			DependencyService.Get<IAudio>().StopPlayingFile();
		}

		void Call112_Clicked(object sender, EventArgs args)
		{
			var phoneCallTask = MessagingPlugin.PhoneDialer;
			if (phoneCallTask.CanMakePhoneCall)
				phoneCallTask.MakePhoneCall("112");
			
		}
	}
}

