﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using Eindwerk.ViewModels;

namespace Eindwerk.View.Pages.Reanimatie
{
	public partial class StabieleZijligging : ContentPage
	{
		//scroll view
		private ScrollView _scrollView;
		//primary titles
		private Label _lblDefinitie, _lblStappenPlan;
		//secondary titles
		private Label _lblVoorBegin, _lblStap1, _lblStap2, _lblStap3, _lblStap4;
		//primary content
		private StackLayout _stckDefinitie, _stckStappenPlan;
		//secondary content
		private StackLayout _stckVoorBegin, _stckStap1, _stckStap2, _stckStap3, _stckStap4;
		private MainViewModel _mainViewModel;
		public StabieleZijligging (MainViewModel viewModel = null)
		{
			if (viewModel != null) {
				_mainViewModel = viewModel;
				this.BindingContext = _mainViewModel;
			}
			//initialise xaml etc
			InitializeComponent ();
			//link instance variables to controls
			_scrollView = this.FindByName<ScrollView> ("scrollView");
			//primary titles
			_lblDefinitie = this.FindByName<Label> ("lblDefinitie");
			_lblStappenPlan = this.FindByName<Label> ("lblStappenPlan");
			//secondary titles
			_lblVoorBegin = this.FindByName<Label> ("lblVoorBegin");
			_lblStap1 = this.FindByName<Label> ("lblStap1");
			_lblStap2 = this.FindByName<Label> ("lblStap2");
			_lblStap3 = this.FindByName<Label> ("lblStap3");
			_lblStap4 = this.FindByName<Label> ("lblStap4");
			//primary content
			_stckDefinitie = this.FindByName<StackLayout> ("stckDefinitie");
			_stckStappenPlan = this.FindByName<StackLayout> ("stckStappenPlan");
			//secondary content
			_stckVoorBegin = this.FindByName<StackLayout> ("stckVoorBegin");
			_stckStap1 = this.FindByName<StackLayout> ("stckStap1");
			_stckStap2 = this.FindByName<StackLayout> ("stckStap2");
			_stckStap3 = this.FindByName<StackLayout> ("stckStap3");
			_stckStap4 = this.FindByName<StackLayout> ("stckStap4");
			//hide content (done here because, if done directly in xaml, dimensions change when isVisible goes to 'false' for the first time)
			_stckDefinitie.IsVisible = false;
			_stckStappenPlan.IsVisible = true ;
            _stckVoorBegin.IsVisible = false;
			_stckStap1.IsVisible = true;            
			_stckStap2.IsVisible = true;
			_stckStap3.IsVisible = true;
			_stckStap4.IsVisible = true;
		}

		void Definitie_Clicked(object sender, EventArgs args)
		{
			if (_lblDefinitie != null && _stckDefinitie != null)
			{
				Show_Or_Hide_Content(_lblDefinitie, _stckDefinitie);
			}
		}

		void StappenPlan_Clicked(object sender, EventArgs args)
		{
			if (_lblStappenPlan != null && _stckStappenPlan != null)
			{
				Show_Or_Hide_Content (_lblStappenPlan, _stckStappenPlan);
			}
		}

		void VoorBegin_Clicked(object sender, EventArgs args)
		{
			if (_lblVoorBegin != null && _stckVoorBegin != null)
			{
				Show_Or_Hide_Content (_lblVoorBegin, _stckVoorBegin);
			}
		}

		void Stap1_Clicked(object sender, EventArgs args)
		{
			if (_lblStap1 != null && _stckStap1 != null)
			{
				Show_Or_Hide_Content (_lblStap1, _stckStap1);
			}
		}

		void Stap2_Clicked(object sender, EventArgs args)
		{
			if (_lblStap2 != null && _stckStap2 != null)
			{
				Show_Or_Hide_Content (_lblStap2, _stckStap2);
			}
		}

		void Stap3_Clicked(object sender, EventArgs args)
		{
			if (_lblStap3 != null && _stckStap3 != null)
			{
				Show_Or_Hide_Content (_lblStap3, _stckStap3);
			}
		}

		void Stap4_Clicked(object sender, EventArgs args)
		{
			if (_lblStap4 != null && _stckStap4 != null)
			{
				Show_Or_Hide_Content (_lblStap4, _stckStap4);
			}
		}

		void Show_Or_Hide_Content(Label titleLabel, StackLayout stackLayout)
		{
			if (!stackLayout.IsVisible)
			{ //show content
				double scrollExtra = (stackLayout.Parent.Parent == _stckStappenPlan ? _stckStappenPlan.Y : 0);
				stackLayout.IsVisible = true;
				titleLabel.Text = "v" + titleLabel.Text.Substring(1);
				_scrollView.ScrollToAsync (0, stackLayout.Y - ((ContentView)titleLabel.Parent).Height + scrollExtra, true);
			}
			else
			{ //hide content
				stackLayout.IsVisible = false;
				titleLabel.Text = ">" + titleLabel.Text.Substring(1);
			}
		}
	}
}

