﻿using System;
using System.Collections.Generic;
using Eindwerk.ViewModels;
using Xamarin.Forms;

namespace Eindwerk.View.Pages.Reanimatie
{
	public partial class HowToReanimate : ContentPage
	{
		//scroll view
		private ScrollView _scrollView;
		//primary titles
		private Label  _lblStappenPlan;
		//secondary titles
		private Label _lblStap1, _lblStap2, _lblStap3, _lblStap4, _lblStap5, _lblStap6, _lblStap7, _lblStap8;
		//primary content
		private StackLayout _stckDefinitie, _stckStappenPlan;
		//secondary content
		private StackLayout _stckVoorBegin, _stckStap1, _stckStap2, _stckStap3, _stckStap4, _stckStap5, _stckStap6, _stckStap7, _stckStap8;
		private MainViewModel _mainViewModel;
		public HowToReanimate (MainViewModel viewModel = null)
		{
			if (viewModel != null) {
				_mainViewModel = viewModel;
				this.BindingContext = _mainViewModel;
			}
			//initialise xaml etc
			InitializeComponent ();

			//link instance variables to controls
			_scrollView = this.FindByName<ScrollView> ("scrollView");

			//primary titles
			_lblStappenPlan = this.FindByName<Label> ("lblStappenPlan");
		
			//secondary titles
			_lblStap1 = this.FindByName<Label> ("lblStap1");
			_lblStap2 = this.FindByName<Label> ("lblStap2");
			_lblStap3 = this.FindByName<Label> ("lblStap3");
			_lblStap1 = this.FindByName<Label> ("lblStap4");
			_lblStap2 = this.FindByName<Label> ("lblStap5");
			_lblStap3 = this.FindByName<Label> ("lblStap6");
			//primary content
			_stckStappenPlan = this.FindByName<StackLayout> ("stckStappenPlan");
			//secondary content

			_stckStap1 = this.FindByName<StackLayout> ("stckStap1");
			_stckStap2 = this.FindByName<StackLayout> ("stckStap2");
			_stckStap3 = this.FindByName<StackLayout> ("stckStap3");
			_stckStap4 = this.FindByName<StackLayout> ("stckStap4");
			_stckStap3 = this.FindByName<StackLayout> ("stckStap5");
			_stckStap4 = this.FindByName<StackLayout> ("stckStap6");
			//hide content (done here because, if done directly in xaml, dimensions change when isVisible goes to 'false' for the first time)
			_stckStappenPlan.IsVisible = true ;
			_stckVoorBegin.IsVisible = false;
			_stckStap1.IsVisible = true;            
			_stckStap2.IsVisible = true;
			_stckStap3.IsVisible = true;
			_stckStap4.IsVisible = true;            
			_stckStap5.IsVisible = true;
			_stckStap6.IsVisible = true;
		}

		void StappenPlan_Clicked(object sender, EventArgs args)
		{
			if (_lblStappenPlan != null && _stckStappenPlan != null)
			{
				Show_Or_Hide_Content (_lblStappenPlan, _stckStappenPlan);
			}
		}
			
		void Stap1_Clicked(object sender, EventArgs args)
		{
			if (_lblStap1 != null && _stckStap1 != null)
			{
				Show_Or_Hide_Content (_lblStap1, _stckStap1);
			}
		}

		void Stap2_Clicked(object sender, EventArgs args)
		{
			if (_lblStap2 != null && _stckStap2 != null)
			{
				Show_Or_Hide_Content (_lblStap2, _stckStap2);
			}
		}

		void Stap3_Clicked(object sender, EventArgs args)
		{
			if (_lblStap3 != null && _stckStap3 != null)
			{
				Show_Or_Hide_Content (_lblStap3, _stckStap3);
			}
		}

		void Stap4_Clicked(object sender, EventArgs args)
		{
			if (_lblStap4 != null && _stckStap4 != null)
			{
				Show_Or_Hide_Content (_lblStap4, _stckStap4);
			}
		}

		void Stap5_Clicked(object sender, EventArgs args)
		{
			if (_lblStap5 != null && _stckStap5 != null)
			{
				Show_Or_Hide_Content (_lblStap5, _stckStap5);
			}
		}

		void Stap6_Clicked(object sender, EventArgs args)
		{
			if (_lblStap6 != null && _stckStap6 != null)
			{
				Show_Or_Hide_Content (_lblStap6, _stckStap6);
			}
		}

		void Show_Or_Hide_Content(Label titleLabel, StackLayout stackLayout)
		{
			if (!stackLayout.IsVisible)
			{ //show content
				double scrollExtra = (stackLayout.Parent.Parent == _stckStappenPlan ? _stckStappenPlan.Y : 0);
				stackLayout.IsVisible = true;
				titleLabel.Text = "v" + titleLabel.Text.Substring(1);
				_scrollView.ScrollToAsync (0, stackLayout.Y - ((ContentView)titleLabel.Parent).Height + scrollExtra, true);
			}
			else
			{ //hide content
				stackLayout.IsVisible = false;
				titleLabel.Text = ">" + titleLabel.Text.Substring(1);
			}
		}
	}
}

