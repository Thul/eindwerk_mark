﻿using Eindwerk.ViewModels;
using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Eindwerk.View.Pages.Kinderen
{
	public partial class Afkapwaardes : ContentPage
	{
        private MainViewModel _viewModel;
		public Afkapwaardes (MainViewModel vm)
		{
            _viewModel = vm;
            this.BindingContext = _viewModel;
            InitializeComponent();
        }
	}
}

