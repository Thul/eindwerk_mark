﻿using System;
using System.Collections.Generic;
using Eindwerk.ViewModels;
using Xamarin.Forms;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using Plugin.Messaging;

namespace Eindwerk.View.Pages.Noodnummers
{
	public partial class NoodnummersFormulier : ContentPage
	{
		Dictionary<string, string> _countries;
		private Button _countryPckr;
		string _selectedCountryCode;
		string[] _countryOptions;
		private ListView _resultLst;
		private Entry _searchEntry;
		public ObservableCollection<KeyValuePair<string, string>> SearchResult { get; set; }
		public MainViewModel MainViewModel { get; set; }

		public NoodnummersFormulier (MainViewModel vm = null)
		{
			SearchResult = new ObservableCollection<KeyValuePair<string, string>> ();
			InitializeComponent ();
			if (vm != null) {
				MainViewModel = vm;
				this.BindingContext = this;
			}
			_selectedCountryCode = "";
			_countryPckr = this.FindByName<Button> ("pkrCountry");
			_resultLst = this.FindByName<ListView> ("lstResultNumbers");
			_searchEntry = this.FindByName<Entry> ("entrySearch");
			_searchEntry.TextChanged += (sender, e) => Search();
			InitPickers ();
		}


		async void PkrCountryClicked(object sender, EventArgs e){
			string result = await DisplayActionSheet ("Selecteer land:", "Annuleren", null, _countryOptions);
			if (result != null && result != "Annuleren") {
				_selectedCountryCode = result.Split (' ') [0];
				((Button)sender).Text = result.Split (' ') [2];
				Search ();
			}
		}
			
		private void InitPickers() {
			_countries = MainViewModel.GetCountries ();
			_countryOptions = new string [_countries.Count()];
			for (int i = 0; i < _countries.Count; i++) {
				_countryOptions[i] = _countries.Keys.ElementAt(i) + " - " + _countries.Values.ElementAt(i);
			}
		}
			
		private void UpdateSearchResults(Dictionary<string, string> phoneNumbers) {
			SearchResult.Clear ();
			foreach(KeyValuePair<string, string> kvp in phoneNumbers) {
				Console.WriteLine (kvp.Key + " " + kvp.Value);
				SearchResult.Add (kvp);
			}
		}

		public void Search() {
			if (_selectedCountryCode.Equals("")) {
				return;
			}
			string keyword = _searchEntry.Text;
			//string _selectedCountryCode = _countryPckr.SelectedIndex>=0?_countryPckr.Items.ElementAt (_countryPckr.SelectedIndex).Split(' ')[0]:"";
			Dictionary<string, string> result = new Dictionary<string, string> ();
			IEnumerable<CountryViewModel> countries = MainViewModel.GetCountryViewModels ();
			int outParse;
			if (keyword != null) {
				UpdateSearchResults (countries.Where ((x) => x.CountryCode == _selectedCountryCode).First ().PhoneNumbers.Where ((x) => x.Name.ToLower ().Contains (keyword.ToLower ())).ToDictionary (item => item.Name, item => item.Number));
			}
		}

		private void NumberClicked(object sender, EventArgs args)
		{
			var textcell = (KeyValuePair<string, string>)(sender as ListView).SelectedItem;
			var phoneCallTask = MessagingPlugin.PhoneDialer;
			if (phoneCallTask.CanMakePhoneCall) phoneCallTask.MakePhoneCall(textcell.Value);
		}
	}
}

