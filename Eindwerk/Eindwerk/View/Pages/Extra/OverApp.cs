﻿using System;
using System.Collections.Generic;
using Eindwerk.ViewModels;
using Xamarin.Forms;

namespace Eindwerk.View.Pages.Extra
{

	public partial class OverApp : ContentPage
	{
		private MainViewModel _mainViewModel;
		public OverApp (MainViewModel mv = null)
		{
			_mainViewModel = mv;
			BindingContext = _mainViewModel;
			InitializeComponent ();
		}
	}
}

