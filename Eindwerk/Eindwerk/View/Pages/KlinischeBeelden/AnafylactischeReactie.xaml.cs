﻿using System;
using System.Collections.Generic;
using Eindwerk.ViewModels;
using Xamarin.Forms;

namespace Eindwerk.View.Pages.KlinischeBeelden
{
    public partial class AnafylactischeReactie : ContentPage
    {
        private ScrollView _scrollView;
        private Label _lblWanneer, _lblLar, _lblEar, _lblAs, _lblErs, _lblBroncho;
        private StackLayout _stckWanneer, _stckLar, _stckEar, _stckAs, _stckErs, _stckBroncho;

        private MainViewModel _articleViewModel;

        public AnafylactischeReactie(MainViewModel vm = null)
        {
            if (vm != null)
            {
                _articleViewModel = vm;
                this.BindingContext = _articleViewModel;
            }
            InitializeComponent();
            _scrollView = this.FindByName<ScrollView>("scrollView");
            _lblWanneer = this.FindByName<Label>("lblWanneer");
            _lblLar = this.FindByName<Label>("lblLar");
            _lblEar = this.FindByName<Label>("lblEar");
            _lblAs = this.FindByName<Label>("lblAs");
            _lblErs = this.FindByName<Label>("lblErs");
            _lblBroncho = this.FindByName<Label>("lblBroncho");
            _stckWanneer = this.FindByName<StackLayout>("stckWanneer");
            _stckLar = this.FindByName<StackLayout>("stckLar");
            _stckEar = this.FindByName<StackLayout>("stckEar");
            _stckAs = this.FindByName<StackLayout>("stckAs");
            _stckErs = this.FindByName<StackLayout>("stckErs");
            _stckBroncho = this.FindByName<StackLayout>("stckBroncho");

            _stckLar.IsVisible = false;
            _stckEar.IsVisible = false;
            _stckBroncho.IsVisible = false;
            _stckAs.IsVisible = false;
            _stckErs.IsVisible = false;

        }

        void Definitie_Clicked(object sender, EventArgs args)
        {
            if (_lblWanneer != null && _stckWanneer != null)
            {
                Show_Or_Hide_Content(_lblWanneer, _stckWanneer);
            }
        }

        void Anticholinerg_Clicked(object sender, EventArgs args)
        {
            if (_lblLar != null && _stckLar != null)
            {
                Show_Or_Hide_Content(_lblLar, _stckLar);
            }
        }

        void Cholinerg_Clicked(object sender, EventArgs args)
        {
            if (_lblEar != null && _stckEar != null)
            {
                Show_Or_Hide_Content(_lblEar, _stckEar);
            }
        }

        void Sympathomimetisch_Clicked(object sender, EventArgs args)
        {
            if (_lblAs != null && _stckAs != null)
            {
                Show_Or_Hide_Content(_lblAs, _stckAs);
            }
        }

        void Alcoholisch_Clicked(object sender, EventArgs args)
        {
            if (_lblErs != null && _stckErs != null)
            {
                Show_Or_Hide_Content(_lblErs, _stckErs);
            }
        }


        void Broncho_Clicked(object sender, EventArgs args)
        {
            if (_lblBroncho != null && _stckBroncho != null)
            {
                Show_Or_Hide_Content(_lblBroncho, _stckBroncho);
            }
        }

        void Show_Or_Hide_Content(Label titleLabel, StackLayout stackLayout)
        {
            if (!stackLayout.IsVisible)
            { //show content
                double scrollExtra = (stackLayout.Parent.Parent == _stckEar ? _stckEar.Y : 0);
                stackLayout.IsVisible = true;
                titleLabel.Text = "v" + titleLabel.Text.Substring(1);
                _scrollView.ScrollToAsync(0, stackLayout.Y - ((ContentView)titleLabel.Parent).Height + scrollExtra, true);
            }
            else
            { //hide content
                stackLayout.IsVisible = false;
                titleLabel.Text = ">" + titleLabel.Text.Substring(1);
            }
        }

    }
}
