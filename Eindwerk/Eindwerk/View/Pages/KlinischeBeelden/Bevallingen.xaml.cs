﻿using System;
using System.Collections.Generic;
using Eindwerk.ViewModels;
using Xamarin.Forms;
using Plugin.Messaging;

namespace Eindwerk.View.Pages.KlinischeBeelden
{
	public partial class Bevallingen : ContentPage
	{
		private ScrollView _scrollView;
		private Label _lblApgar, _lblBevalling, _lblHoofd, _lblStuit;
		private StackLayout _stckApgar, _stckBevalling, _stckHoofd, _stckStuit;
		private MainViewModel _mainViewModel;
		public Bevallingen (MainViewModel vm = null)
		{
			if (vm != null) {
				_mainViewModel = vm;
				this.BindingContext = _mainViewModel;
			}
			//initialise xaml etc
			InitializeComponent ();
			//link instance variables to controls
			_scrollView = this.FindByName<ScrollView>("scrollView");
			//titles
			_lblApgar= this.FindByName<Label> ("lblApgar");
			_lblBevalling = this.FindByName<Label> ("lblBevalling");
			_lblHoofd = this.FindByName<Label> ("lblHoofd");
			_lblStuit = this.FindByName<Label> ("lblStuit");
			//content
			_stckApgar = this.FindByName<StackLayout> ("stckApgar");
			_stckBevalling = this.FindByName<StackLayout>("stckBevalling");
			_stckHoofd = this.FindByName<StackLayout> ("stckHoofd");
			_stckStuit = this.FindByName<StackLayout> ("stckStuit");
			//hide content (done here because, if done directly in xaml, dimensions change when isVisible goes to 'false' for the first time)
			_stckApgar.IsVisible = false;
			_stckHoofd.IsVisible = false;
			_stckStuit.IsVisible = false;
		}

		void Bevalling_Clicked(object sender, EventArgs args)
		{
			if (_lblBevalling != null && _stckBevalling != null)
			{
				Show_Or_Hide_Content(_lblBevalling, _stckBevalling);
			}
		}

		void Hoofd_Clicked(object sender, EventArgs args)
		{
			if (_lblHoofd != null && _stckHoofd != null)
			{
				Show_Or_Hide_Content(_lblHoofd, _stckHoofd);
			}
		}

		void Stuit_Clicked(object sender, EventArgs args)
		{
			if (_lblStuit != null && _stckStuit != null)
			{
				Show_Or_Hide_Content(_lblStuit, _stckStuit);
			}
		}

		void Apgar_Clicked(object sender, EventArgs args)
		{
			if (_lblApgar != null && _stckApgar != null)
			{
				Show_Or_Hide_Content(_lblApgar, _stckApgar);
			}
		}

		void Show_Or_Hide_Content(Label titleLabel, StackLayout stackLayout)
		{
			if (!stackLayout.IsVisible)
			{ //show content
				stackLayout.IsVisible = true;
				titleLabel.Text = "v" + titleLabel.Text.Substring(1);
				_scrollView.ScrollToAsync (0, stackLayout.Y - ((ContentView)titleLabel.Parent).Height, true);
			}
			else
			{ //hide content
				stackLayout.IsVisible = false;
				titleLabel.Text = ">" + titleLabel.Text.Substring(1);
			}
		}


	}
}

