﻿using System;
using System.Collections.Generic;
using Eindwerk.ViewModels;
using Xamarin.Forms;

namespace Eindwerk.View.Pages.KlinischeBeelden
{
	public partial class PijnOpBorst : ContentPage
	{
		private MainViewModel _mainViewModel;
		public PijnOpBorst (MainViewModel vm = null)
		{
			if (vm != null) {
				_mainViewModel = vm;
				this.BindingContext = _mainViewModel;
			}
			InitializeComponent ();
		}
	}
}

