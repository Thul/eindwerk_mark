﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Eindwerk.ViewModels;
using Xamarin.Forms;

namespace Eindwerk.View.Pages.KlinischeBeelden
{
	public partial class Toxidromen : ContentPage
	{
		private ScrollView _scrollView;
		private Label _lblDefinitie, _lblAnticholinerg, _lblOpvThuis, _lblCholinerg, _lblSympathomimetisch, _lblAlcoholisch, _lblSerotine;
		private StackLayout _stckDefinitie, _stckAnticholinerg, _stckCholinerg, _stckSympathomimetisch, _stckAlcoholisch, _stckSerotine;

		private Grid _gridAnticholinergSympt, _gridAnticholinergOorzaken, _gridCholinergSympt, _gridCholinergOorzaken, _gridSympathomimetischSympt, _gridSympathomimetischOorzaken, _gridAlcoholischOorzaken, _gridAlcoholischSympt, _gridSerotineSympt, _gridSerotineOorzaken;
		private MainViewModel _mainViewModel;


		public Toxidromen(MainViewModel vm = null)
		{
			if (vm != null) {
				_mainViewModel = vm;
				this.BindingContext = _mainViewModel;
			}
			//initialise xaml etc.
			InitializeComponent();
			//link instance variables to controls
			_scrollView = this.FindByName<ScrollView>("scrollView");

			_lblDefinitie = this.FindByName<Label>("lblDefinitie");
			_lblAnticholinerg = this.FindByName<Label>("lblAnticholinerg");
			_lblCholinerg = this.FindByName<Label> ("lblCholinerg");
			_lblSympathomimetisch = this.FindByName<Label> ("lblSympathomimetisch");
			_lblAlcoholisch = this.FindByName<Label> ("lblAlcoholisch");
			_lblSerotine = this.FindByName<Label> ("lblSerotine");
			_lblOpvThuis = this.FindByName<Label>("lblOpvThuis");

			_stckDefinitie = this.FindByName<StackLayout>("stckDefinitie");
			_stckAnticholinerg = this.FindByName<StackLayout>("stckAnticholinerg");
			_stckCholinerg = this.FindByName<StackLayout>("stckCholinerg");
			_stckSympathomimetisch = this.FindByName<StackLayout> ("stckSympathomimetisch");
			_stckAlcoholisch = this.FindByName<StackLayout> ("stckAlcoholisch");
			_stckSerotine = this.FindByName<StackLayout> ("stckSerotine");

			_gridAnticholinergSympt = this.FindByName<Grid> ("gridAnticholinergSympt");
			_gridAnticholinergOorzaken = this.FindByName<Grid> ("gridAnticholinergOorzaken");

			_gridCholinergSympt = this.FindByName<Grid> ("gridCholinergSympt");
			_gridCholinergOorzaken = this.FindByName<Grid> ("gridCholinergOorzaken");

			_gridSympathomimetischSympt = this.FindByName<Grid> ("gridSympathomimetischSympt");
			_gridSympathomimetischOorzaken = this.FindByName<Grid> ("gridSympathomimetischOorzaken");

			_gridAlcoholischSympt = this.FindByName<Grid> ("gridAlcoholischSympt");
			_gridAlcoholischOorzaken = this.FindByName<Grid> ("gridAlcoholischOorzaken");

			_gridSerotineSympt = this.FindByName<Grid> ("gridSerotineSympt");
			_gridSerotineOorzaken = this.FindByName<Grid> ("gridSerotineOorzaken");

			//hide content (done here because - if done directly in xaml - dimensions change when isVisible goes to 'false' for the first time)

			_stckAnticholinerg.IsVisible = false;
			_gridAnticholinergSympt.IsVisible = true;
			_gridAnticholinergOorzaken.IsVisible = false;

			_stckCholinerg.IsVisible = false;
			_gridCholinergSympt.IsVisible = true;
			_gridCholinergOorzaken.IsVisible = false;

			_stckSympathomimetisch.IsVisible = false;
			_gridSympathomimetischSympt.IsVisible = true;
			_gridSympathomimetischOorzaken.IsVisible = false;

			_stckAlcoholisch.IsVisible = false;
			_gridAlcoholischSympt.IsVisible = true;
			_gridAlcoholischOorzaken.IsVisible = false;

			_stckSerotine.IsVisible = false;
			_gridSerotineSympt.IsVisible = true;
			_gridSerotineOorzaken.IsVisible = false;
		}

		void Definitie_Clicked(object sender, EventArgs args)
		{
			if (_lblDefinitie != null && _stckDefinitie != null)
			{
				Show_Or_Hide_Content(_lblDefinitie, _stckDefinitie);
			}
		}

		void Anticholinerg_Clicked(object sender, EventArgs args)
		{
			if (_lblAnticholinerg != null && _stckAnticholinerg != null)
			{
				Show_Or_Hide_Content(_lblAnticholinerg, _stckAnticholinerg);
			}
		}


		void Cholinerg_Clicked(object sender, EventArgs args)
		{
        		if (_lblCholinerg != null && _stckCholinerg != null)
			{
				Show_Or_Hide_Content(_lblCholinerg, _stckCholinerg);
			}
		}

		void Sympathomimetisch_Clicked(object sender, EventArgs args)
		{
			if (_lblSympathomimetisch != null && _stckSympathomimetisch != null)
			{
				Show_Or_Hide_Content(_lblSympathomimetisch, _stckSympathomimetisch);
			}
		}

		void Alcoholisch_Clicked(object sender, EventArgs args)
		{
			if (_lblAlcoholisch != null && _stckAlcoholisch != null)
			{
				Show_Or_Hide_Content(_lblAlcoholisch, _stckAlcoholisch);
			}
		}


        void Serotine_Clicked(object sender, EventArgs args)
		{
			if (_lblSerotine != null && _stckSerotine != null)
			{
				Show_Or_Hide_Content(_lblSerotine, _stckSerotine);
			}
		}


		void Show_Or_Hide_Content(Label titleLabel, StackLayout stackLayout)
		{
			if (!stackLayout.IsVisible)
			{ //show content
				stackLayout.IsVisible = true;
				titleLabel.Text = "v" + titleLabel.Text.Substring(1);
				_scrollView.ScrollToAsync (0, stackLayout.Y - ((ContentView)titleLabel.Parent).Height, true);
			}
			else
			{ //hide content
				stackLayout.IsVisible = false;
				titleLabel.Text = ">" + titleLabel.Text.Substring(1);
			}
		}
		
		void Sympt_Anticholinerg_Clicked(object sender, EventArgs args) {
			_gridAnticholinergSympt.IsVisible = true;
			_gridAnticholinergOorzaken.IsVisible = false;
		}

		void Oorzaken_Anticholinerg_Clicked(object sender, EventArgs args) {
			_gridAnticholinergSympt.IsVisible = false;
			_gridAnticholinergOorzaken.IsVisible = true;
		}

		void Sympt_Cholinerg_Clicked(object sender, EventArgs args) {
			_gridCholinergSympt.IsVisible = true;
			_gridCholinergOorzaken.IsVisible = false;
		}

		void Oorzaken_Cholinerg_Clicked(object sender, EventArgs args) {
			_gridCholinergSympt.IsVisible = false;
			_gridCholinergOorzaken.IsVisible = true;
		}

		void Sympt_Sympathomimetisch_Clicked(object sender, EventArgs args) {
			_gridSympathomimetischSympt.IsVisible = true;
			_gridSympathomimetischOorzaken.IsVisible = false;
		}

		void Oorzaken_Sympathomimetisch_Clicked(object sender, EventArgs args) {
			_gridSympathomimetischSympt.IsVisible = false;
			_gridSympathomimetischOorzaken.IsVisible = true;
		}

		void Sympt_Alcoholisch_Clicked(object sender, EventArgs args) {
			_gridAlcoholischSympt.IsVisible = true;
			_gridAlcoholischOorzaken.IsVisible = false;
		}

		void Oorzaken_Alcoholisch_Clicked(object sender, EventArgs args) {
			_gridAlcoholischSympt.IsVisible = false;
			_gridAlcoholischOorzaken.IsVisible = true;
		}

		void Sympt_Serotine_Clicked(object sender, EventArgs args) {
			_gridSerotineSympt.IsVisible = true;
			_gridSerotineOorzaken.IsVisible = false;
		}

		void Oorzaken_Serotine_Clicked(object sender, EventArgs args) {
			_gridSerotineSympt.IsVisible = false;
			_gridSerotineOorzaken.IsVisible = true;
		}


	}

   }