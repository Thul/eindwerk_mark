﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Eindwerk.ViewModels;
using Xamarin.Forms;
using Plugin.Messaging;

namespace Eindwerk.View.Pages.KlinischeBeelden
{
	public partial class MTBrainInjury : ContentPage
	{
		
		private ScrollView _scrollView;
		private Label _lblDefinitie, _lblBeeldvOpname, _lblOpvThuis;
		private StackLayout _stckDefinitie, _stckBeeldvOpname, _stckOpvThuis;
		private MainViewModel _mainViewModel;

		public MTBrainInjury (MainViewModel viewModel = null)
		{		
			if (viewModel != null) {
				_mainViewModel = viewModel;
				this.BindingContext = _mainViewModel;
			}
			//initialise xaml etc.
			InitializeComponent ();
			//link instance variables to controls
			_scrollView = this.FindByName<ScrollView> ("scrollView");
			_lblDefinitie = this.FindByName<Label> ("lblDefinitie");
			_lblBeeldvOpname = this.FindByName<Label> ("lblBeeldvOpname");
			_lblOpvThuis = this.FindByName<Label> ("lblOpvThuis");
			_stckDefinitie = this.FindByName<StackLayout> ("stckDefinitie");
			_stckBeeldvOpname = this.FindByName<StackLayout> ("stckBeeldvOpname");
			_stckOpvThuis = this.FindByName<StackLayout> ("stckOpvThuis");
			//hide content (done here because, if done directly in xaml, dimensions change when isVisible goes to 'false' for the first time)
			_stckBeeldvOpname.IsVisible = false;
			_stckOpvThuis.IsVisible = false;
		}

		void Definitie_Clicked (object sender, EventArgs args)
		{
			if (_lblDefinitie != null && _stckDefinitie != null) {
				Show_Or_Hide_Content (_lblDefinitie, _stckDefinitie);
			}
		}

		void BeeldvOpname_Clicked (object sender, EventArgs args)
		{
			if (_lblBeeldvOpname != null && _stckBeeldvOpname != null) {
				Show_Or_Hide_Content (_lblBeeldvOpname, _stckBeeldvOpname);
			}
		}

		void OpvThuis_Clicked (object sender, EventArgs args)
		{
			if (_lblOpvThuis != null && _stckOpvThuis != null) {
				Show_Or_Hide_Content (_lblOpvThuis, _stckOpvThuis);
			}
		}

		void Show_Or_Hide_Content (Label titleLabel, StackLayout stackLayout)
		{
			if (!stackLayout.IsVisible) { //show content
				stackLayout.IsVisible = true;
				
				titleLabel.Text = "v" + titleLabel.Text.Substring (1);
				if (stackLayout != _stckDefinitie) //animation if one of the bottom stackpanels
										_scrollView.ScrollToAsync (0, stackLayout.Y, true);
			} else { //hide content
				stackLayout.IsVisible = false;
				titleLabel.Text = ">" + titleLabel.Text.Substring (1);
			}
		}
	}
}
