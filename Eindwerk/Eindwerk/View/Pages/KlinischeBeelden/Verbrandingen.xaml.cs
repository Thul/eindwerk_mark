﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Eindwerk.ViewModels;
using Xamarin.Forms;

namespace Eindwerk.View.Pages.KlinischeBeelden
{
	public partial class Verbrandingen : ContentPage
	{
		private ScrollView _scrollView;
		private Label _lblDefinitie, _lblBeeldvOpname;
		private StackLayout _stckDefinitie, _stckBeeldvOpname;
		private MainViewModel _mainViewModel;

		public Verbrandingen(MainViewModel viewModel = null)
		{
			if (viewModel != null) {
				_mainViewModel = viewModel;
				this.BindingContext = _mainViewModel;
			}
			//initialise xaml etc.
			InitializeComponent();
			//link instance variables to controls
			_scrollView = this.FindByName<ScrollView>("scrollView");
			_lblDefinitie = this.FindByName<Label>("lblDefinitie");
			_lblBeeldvOpname = this.FindByName<Label>("lblBeeldvOpname");
			_stckDefinitie = this.FindByName<StackLayout>("stckDefinitie");
			_stckBeeldvOpname = this.FindByName<StackLayout>("stckBeeldvOpname");
			//hide content (done here because - if done directly in xaml - dimensions change when isVisible goes to 'false' for the first time)
			_stckBeeldvOpname.IsVisible = false;
		}

		void Definitie_Clicked(object sender, EventArgs args)
		{
			if (_lblDefinitie != null && _stckDefinitie != null)
			{
				Show_Or_Hide_Content(_lblDefinitie, _stckDefinitie);
			}
		}

		void BeeldvOpname_Clicked(object sender, EventArgs args)
		{
			if (_lblBeeldvOpname != null && _stckBeeldvOpname != null)
			{
				Show_Or_Hide_Content(_lblBeeldvOpname, _stckBeeldvOpname);
			}
		}


		void Show_Or_Hide_Content(Label titleLabel, StackLayout stackLayout)
		{
			if (!stackLayout.IsVisible)
			{ //show content
				stackLayout.IsVisible = true;
				titleLabel.Text = "v" + titleLabel.Text.Substring(1);
				_scrollView.ScrollToAsync(0, stackLayout.Y - ((ContentView)titleLabel.Parent).Height, true);
			}
			else
			{ //hide content
				stackLayout.IsVisible = false;
				titleLabel.Text = ">" + titleLabel.Text.Substring(1);
			}
		}
	}
}
