﻿#if __ANDROID__

using System;
using System.Collections.Generic;
using System.Text;

using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

using Eindwerk.View.Renderers;

[assembly: ExportRenderer(typeof(Picker), typeof(PickerRenderer_NoSpellingCheck))]

namespace Eindwerk.View.Renderers
{
    class PickerRenderer_NoSpellingCheck : PickerRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Picker> e)
        {
            base.OnElementChanged(e);
            if (e.OldElement == null)
            {
				Control.InputType = Android.Text.InputTypes.TextFlagNoSuggestions;
				Control.SetTextColor (Android.Content.Res.ColorStateList.ValueOf(Android.Graphics.Color.Black));
            }
        }
    }
}

#endif