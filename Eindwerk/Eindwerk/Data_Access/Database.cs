﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Eindwerk.Data_Access
{
	public interface DatabaseBase {}

	/// <summary>
	/// Generic interface for all database operations
	/// </summary>
	public interface Database<T> : DatabaseBase
    {
        List<T> GetItems();
        T GetItem(int id);
        void SaveItem(T t);
        int DeleteItem(int id);
        int DeleteAll();
        void UpdateItem(T t);
    }
}
