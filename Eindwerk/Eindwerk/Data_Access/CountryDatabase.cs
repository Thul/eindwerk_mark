﻿using Eindwerk.Business.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using SQLite;
using System.Linq;
using System.IO;
using Eindwerk.Data;

namespace Eindwerk.Data_Access
{
    public class CountryDatabase : Database<Country>
    {

        static object locker = new object();

        SQLiteConnection database;

		/// <summary>
		/// Get path to database, on every platform
		/// </summary>
        string DatabasePath
        {
            get
            {
				var sqliteFilename = DbManager.DB_LOCATION;
				#if __IOS__
                string documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal); // Documents folder
                string libraryPath = Path.Combine(documentsPath, "..", "Library"); // Library folder
                var path = Path.Combine(libraryPath, sqliteFilename);
				#else
				#if __ANDROID__
				string documentsPath = Environment.GetFolderPath (Environment.SpecialFolder.Personal); // Documents folder
				var path = Path.Combine(documentsPath, sqliteFilename);
				#endif
				#endif
                return path;
            }
        }

		/// <summary>
		/// Connect and create country table if non-existant
		/// </summary>
        public CountryDatabase()
        {
            database = new SQLiteConnection(DatabasePath);
            database.CreateTable<Country>();
        }
		 
		/// <summary>
		/// Deletes all countries from the database
		/// </summary>
        public int DeleteAll()
        {
            lock (locker)
            {
                return database.DeleteAll<Country>();
            }
        }

		/// <summary>
		/// Deletes country from the database
		/// </summary>
		/// <param name="id">Specifies id of the country</param>
		/// <returns>Integer succeeded</returns>
        public int DeleteItem(int id)
        {
            lock (locker)
            {
                return database.Delete(id);
            }
        }

		/// <summary>
		/// Retrieves country from the database
		/// </summary>
		/// <param name="id">Specifies id of the country</param>
		/// <returns>Country entity object</returns>
        public Country GetItem(int id)
        {
            lock (locker)
            {
				return database.Table<Country>().FirstOrDefault(x => x.Id == id);
            }
        }

		/// <summary>
		/// Retrieves all countries from the database
		/// </summary>
		/// <returns>List of countries</returns>
        public List<Country> GetItems()
        {
            lock (locker)
            {
                return (from i in database.Table<Country>() select i).ToList();
            }
        }

		/// <summary>
		/// Stores article in the database
		/// </summary>
		/// <param name="t">Article</param>
        public void SaveItem(Country t)
        {
            lock (locker)
            {
                database.Insert(t);
            }
        }

		/// <summary>
		/// Updates country in the database
		/// </summary>
		/// <param name="t">Country</param>
        public void UpdateItem(Country t)
        {
            lock (locker)
            {
                database.Update(t);
            }
        }
    }
}
