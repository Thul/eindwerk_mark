﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using SQLite;
using Eindwerk.Business.Entities;
using System.Linq;
using Eindwerk.Data;
namespace Eindwerk.Data_Access
{
    public class TagDatabase : Database<Tag>
    {


        static object locker = new object();

        SQLiteConnection database;

		/// <summary>
		/// Get path to database, on every platform
		/// </summary>
        string DatabasePath
        {
            get
            {
				var sqliteFilename = DbManager.DB_LOCATION;;
                #if __IOS__
                string documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal); // Documents folder
                string libraryPath = Path.Combine(documentsPath, "..", "Library"); // Library folder
                var path = Path.Combine(libraryPath, sqliteFilename);
                #else
                #if __ANDROID__
				string documentsPath = Environment.GetFolderPath (Environment.SpecialFolder.Personal); // Documents folder
				var path = Path.Combine(documentsPath, sqliteFilename);
                #else
				// WinPhone
				var path = Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path, sqliteFilename);;
                #endif
                #endif
                return path;
            }
        }

		/// <summary>
		/// Connect and create tag table if non-existant
		/// </summary>
        public TagDatabase()
        {
            database = new SQLiteConnection(DatabasePath);
            database.CreateTable<Tag>();
        }

		/// <summary>
		/// Deletes all tags from the database
		/// </summary>
        public int DeleteAll()
        {
            lock (locker)
            {
                return database.DeleteAll<Tag>();
            }
        }

		/// <summary>
		/// Deletes tag from the database
		/// </summary>
		/// <param name="id">Specifies id of the tag</param>
		/// <returns>Integer succeeded</returns>
        public int DeleteItem(int id)
        {
            lock (locker)
            {
                return database.Delete(id);
            }
        }

		/// <summary>
		/// Retrieves tag from the database
		/// </summary>
		/// <param name="id">Specifies id of the tag</param>
		/// <returns>Tag entity object</returns>
        public Tag GetItem(int id)
        {
            lock (locker)
            {
                return database.Table<Tag>().FirstOrDefault(x => x.Id == id);
            }
        }

		/// <summary>
		/// Retrieves all tags from the database
		/// </summary>
		/// <returns>List of tags</returns>
        public List<Tag> GetItems()
        {
            lock (locker)
            {
                return (from i in database.Table<Tag>() select i).ToList();
            }
        }

		/// <summary>
		/// Stores tag in the database
		/// </summary>
		/// <param name="t">Article</param>
        public void SaveItem(Tag t)
        {
            lock (locker)
            {
                database.Insert(t);
            }
        }

		/// <summary>
		/// Updates tag in the database
		/// </summary>
		/// <param name="t">Tag</param>
        public void UpdateItem(Tag t)
        {
            lock (locker)
            {
                database.Update(t);
            }
        }
    }
}
