﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Linq;
using SQLite;
using Eindwerk.Business.Entities;
using Eindwerk.Data;
namespace Eindwerk.Data_Access
{
    public class PhoneNumberDatabase : Database<PhoneNumber>
    {
        static object locker = new object();

        SQLiteConnection database;

		/// <summary>
		/// Get path to database, on every platform
		/// </summary>
        string DatabasePath
        {
            get
            {
				var sqliteFilename = DbManager.DB_LOCATION;
                #if __IOS__
                string documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal); // Documents folder
                string libraryPath = Path.Combine(documentsPath, "..", "Library"); // Library folder
                var path = Path.Combine(libraryPath, sqliteFilename);
                #else
                #if __ANDROID__
				string documentsPath = Environment.GetFolderPath (Environment.SpecialFolder.Personal); // Documents folder
				var path = Path.Combine(documentsPath, sqliteFilename);
                #endif
                #endif
                return path;
            }
        }

		/// <summary>
		/// Connect and create phonenumber table if non-existant
		/// </summary>
        public PhoneNumberDatabase()
        {
            database = new SQLiteConnection(DatabasePath);
            database.CreateTable<PhoneNumber>();
        }

		/// <summary>
		/// Deletes all phonenumbers from the database
		/// </summary>
        public int DeleteAll()
        {
            lock (locker)
            {
                return database.DeleteAll<PhoneNumber>();
            }
        }

		/// <summary>
		/// Deletes phonenumber from the database
		/// </summary>
		/// <param name="id">Specifies id of the phonenumber</param>
		/// <returns>Integer succeeded</returns>
        public int DeleteItem(int id)
        {
            lock (locker)
            {
                return database.Delete<PhoneNumber>(id);
            }
        }

		/// <summary>
		/// Retrieves phonenumber from the database
		/// </summary>
		/// <param name="id">Specifies id of the phonenumber</param>
		/// <returns>PhoneNumber entity object</returns>
        public PhoneNumber GetItem(int id)
        {
            lock (locker)
            {
                return database.Table<PhoneNumber>().FirstOrDefault(x => x.Id == id);
            }
        }

		/// <summary>
		/// Retrieves all phonenumbers from the database
		/// </summary>
		/// <returns>List of phonenumbers</returns>
        public List<PhoneNumber> GetItems()
        {
            lock (locker)
            {
                return (from i in database.Table<PhoneNumber>() select i).ToList();
            }
        }

		/// <summary>
		/// Stores phonenumber the database
		/// </summary>
		/// <param name="t">Article</param>
        public void SaveItem(PhoneNumber t)
        {
            lock (locker)
            {
                database.Insert(t);
            }
        }

		/// <summary>
		/// Updates phonenumber in the database
		/// </summary>
		/// <param name="t">Phonenumber</param>
        public void UpdateItem(PhoneNumber t)
        {
            lock (locker)
            {
                database.Update(t);
            }
        }
    }
}
