﻿using System;
using System.Collections.Generic;
using System.Linq;
using Eindwerk.Data_Access.Proxy;
namespace Eindwerk.Data_Access
{
	public static class DatabaseFactory<T> 
	{
		private static Dictionary<string, DatabaseBase> _databases;

		static DatabaseFactory ()
		{
			_databases = new Dictionary<string, DatabaseBase> ();
			_databases.Add ("Article", new ArticleDatabaseProxy ());
			_databases.Add ("Country", new CountryDatabaseProxy ());
			_databases.Add ("PhoneNumber", new PhoneNumberDatabaseProxy ());
			_databases.Add ("Province", new ProvinceDatabaseProxy ());
			_databases.Add ("Tag", new TagDatabaseProxy ());
			_databases.Add ("Category", new CategoryDatabaseProxy ());
		}

		public static Database<T> CreateDatabase()  
		{
			Type t = typeof(T);
			return (Database<T>)_databases [t.Name];
		}

	}
}

