﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;
using System.Linq;
using System.IO;
using Eindwerk.Business.Entities;
using Eindwerk.Data;
namespace Eindwerk.Data_Access
{
    public class ProvinceDatabase : Database<Province>
    {
        static object locker = new object();

        SQLiteConnection database;

		/// <summary>
		/// Get path to database, on every platform
		/// </summary>
        string DatabasePath
        {
            get
            {
				var sqliteFilename = DbManager.DB_LOCATION;;
				#if __IOS__
                string documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal); // Documents folder
                string libraryPath = Path.Combine(documentsPath, "..", "Library"); // Library folder
                var path = Path.Combine(libraryPath, sqliteFilename);
				#else
				#if __ANDROID__
				string documentsPath = Environment.GetFolderPath (Environment.SpecialFolder.Personal); // Documents folder
				var path = Path.Combine(documentsPath, sqliteFilename);
				#endif
				#endif
                return path;
            }
        }

		/// <summary>
		/// Connect and create province table if non-existant
		/// </summary>
        public ProvinceDatabase()
        {
            database = new SQLiteConnection(DatabasePath);
            database.CreateTable<Province>();
        }

		/// <summary>
		/// Deletes all provinces from the database
		/// </summary>
        public int DeleteAll()
        {
            lock (locker)
            {
                return database.DeleteAll<Province>();
            }
        }

		/// <summary>
		/// Deletes province from the database
		/// </summary>
		/// <param name="id">Specifies id of the province</param>
		/// <returns>Integer succeeded</returns>
        public int DeleteItem(int id)
        {
            lock (locker)
            {
                return database.Delete<Province>(id);
            }
        }

		/// <summary>
		/// Retrieves province from the database
		/// </summary>
		/// <param name="id">Specifies id of the province</param>
		/// <returns>Province entity object</returns>
        public Province GetItem(int id)
        {
            lock (locker)
            {
                return database.Table<Province>().FirstOrDefault(x => x.Id == id);
            }
        }

		/// <summary>
		/// Retrieves all provinces from the database
		/// </summary>
		/// <returns>List of provinces</returns>
        public List<Province> GetItems()
        {
            lock (locker)
            {
                return (from i in database.Table<Province>() select i).ToList();
            }
        }

		/// <summary>
		/// Stores province the database
		/// </summary>
		/// <param name="t">Province</param>
        public void SaveItem(Province t)
        {
            lock (locker)
            {

                database.Insert(t);
            }
        }

		/// <summary>
		/// Updates province in the database
		/// </summary>
		/// <param name="t">Province</param>
        public void UpdateItem(Province t)
        {
            lock (locker)
            {
                database.Update(t);
            }
        }
    }
}
