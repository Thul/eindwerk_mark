﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;
using System.Linq;
using System.IO;
using Eindwerk.Business.Entities;

namespace Eindwerk.Data_Access
{
	public class ArticleDatabase : Database<Article>
	{

		static object locker = new object ();

		SQLiteConnection database;

		/// <summary>
		/// Get path to database, on every platform
		/// </summary>
		string DatabasePath {
			get {
				var sqliteFilename = "SQLiteEindwerk.db3";
				#if __IOS__
                string documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal); // Documents folder
                string libraryPath = Path.Combine(documentsPath, "..", "Library"); // Library folder
                var path = Path.Combine(libraryPath, sqliteFilename);
				#else
				#if __ANDROID__
				string documentsPath = Environment.GetFolderPath (Environment.SpecialFolder.Personal); // Documents folder
				var path = Path.Combine (documentsPath, sqliteFilename);
				#endif
				#endif
				return path;
			}
		}

		/// <summary>
		/// Connect and create article table if non-existant
		/// </summary>
		public ArticleDatabase ()
		{
			database = new SQLiteConnection (DatabasePath);
			database.CreateTable<Article> ();
		}

		/// <summary>
		/// Deletes all articles from the database
		/// </summary>
		public int DeleteAll ()
		{
			return database.DeleteAll<Article> ();
		}

		/// <summary>
		/// Deletes article from the database
		/// </summary>
		/// <param name="id">Specifies id of the article</param>
		/// <returns>Integer succeeded</returns>
		public int DeleteItem (int id)
		{
			lock (locker) {
				return database.Delete<Article> (id);
			}
		}

		/// <summary>
		/// Retrieves article from the database
		/// </summary>
		/// <param name="id">Specifies id of the article</param>
		/// <returns>Article entity object</returns>
		public Article GetItem (int id)
		{
			lock (locker) {
				return database.Table<Article> ().FirstOrDefault (x => x.Id == id);
			}
		}

		/// <summary>
		/// Retrieves all articles from the database
		/// </summary>
		/// <returns>List of Articles</returns>
		public List<Article> GetItems ()
		{
			lock (locker) {
				return (from i in database.Table<Article> ()
				                    select i).ToList ();
			}
		}

		/// <summary>
		/// Stores article the database
		/// </summary>
		/// <param name="t">Article</param>
		public void SaveItem (Article t)
		{
			lock (locker) {
				database.Insert (t);
			}
		}

		/// <summary>
		/// Updates article in the database
		/// </summary>
		/// <param name="t">Article</param>
		public void UpdateItem (Article t)
		{
			lock (locker) {
				database.Update (t);
			}
		}
	}
}
