﻿using System;
using Eindwerk.Business.Entities;
using SQLite;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using Eindwerk.Data;
namespace Eindwerk.Data_Access
{
	public class CategoryDatabase:Database<Category>
	{
		static object locker = new object();

		SQLiteConnection database;

		/// <summary>
		/// Get path to database, on every platform
		/// </summary>
		string DatabasePath
		{
			get
			{
				var sqliteFilename = DbManager.DB_LOCATION;
				#if __IOS__
				string documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal); // Documents folder
				string libraryPath = Path.Combine(documentsPath, "..", "Library"); // Library folder
				var path = Path.Combine(libraryPath, sqliteFilename);
				#else
				#if __ANDROID__
				string documentsPath = Environment.GetFolderPath (Environment.SpecialFolder.Personal); // Documents folder
				var path = Path.Combine(documentsPath, sqliteFilename);
				#endif
				#endif
				return path;
			}
		}

		/// <summary>
		/// Connect and create category table if non-existant
		/// </summary>
		public CategoryDatabase()
		{
			database = new SQLiteConnection(DatabasePath);
			database.CreateTable<Category>();
		}

		/// <summary>
		/// Deletes all categories from the database
		/// </summary>
		public int DeleteAll()
		{
			lock (locker)
			{
				return database.DeleteAll<Category>();
			}
		}

		/// <summary>
		/// Deletes category from the database
		/// </summary>
		/// <param name="id">Specifies id of the category</param>
		/// <returns>Integer succeeded</returns>
		public int DeleteItem(int id)
		{
			lock (locker)
			{
				return database.Delete(id);
			}
		}

		/// <summary>
		/// Retrieves cetegory from the database
		/// </summary>
		/// <param name="id">Specifies id of the category</param>
		/// <returns>Article entity object</returns>
		public Category GetItem(int id)
		{
			lock (locker)
			{
				return database.Table<Category>().FirstOrDefault(x => x.Id == id);
			}
		}

		/// <summary>
		/// Retrieves all categories from the database
		/// </summary>
		/// <returns>List of categories</returns>
		public List<Category> GetItems()
		{
			lock (locker)
			{
				return (from i in database.Table<Category>() select i).ToList();
			}
		}

		/// <summary>
		/// Stores category to the database
		/// </summary>
		/// <param name="t">Category</param>
		public void SaveItem(Category t)
		{
			lock (locker)
			{
				database.Insert(t);
			}
		}

		/// <summary>
		/// Updates category in the database
		/// </summary>
		/// <param name="t">Category</param>
		public void UpdateItem(Category t)
		{
			lock (locker)
			{
				database.Update(t);
			}
		}
	}
}

