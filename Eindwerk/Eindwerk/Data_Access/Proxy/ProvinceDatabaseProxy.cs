﻿using System;
using Eindwerk.Business.Entities;
using Eindwerk.Data_Access;
using System.Collections.Generic;

namespace Eindwerk.Data_Access.Proxy
{
	/// <summary>
	/// Proxy for the lazy loading of the provincedb in the factory
	/// View documentation of ProvinceDatabase for an explanation of the different methods
	/// </summary>
	public class ProvinceDatabaseProxy : Database<Province>
	{
		private ProvinceDatabase _provinceDb;
		public ProvinceDatabaseProxy () { }

		private ProvinceDatabase loadDb() {
			if (_provinceDb== null) 
			{
				_provinceDb = new ProvinceDatabase ();
			}
			return _provinceDb;
		}

		public int DeleteAll()
		{
			return loadDb().DeleteAll ();
		}


		public int DeleteItem(int id)
		{
			return loadDb ().DeleteItem (id);
		}

		public Province GetItem(int id)
		{
			return loadDb ().GetItem (id);
		}

		public List<Province> GetItems()
		{
			return loadDb ().GetItems ();
		}

		public void SaveItem(Province t)
		{
			loadDb ().SaveItem (t);
		}

		public void UpdateItem(Province t)
		{
			loadDb ().UpdateItem (t);
		}
	}
}

