﻿using System;
using Eindwerk.Business.Entities;
using Eindwerk.Data_Access;
using System.Collections.Generic;

namespace Eindwerk.Data_Access.Proxy
{
	/// <summary>
	/// Proxy for the lazy loading of the phonenumberdb in the factory
	/// View documentation of PhoneNumberDatabase for an explanation of the different methods
	/// </summary>
	public class PhoneNumberDatabaseProxy : Database<PhoneNumber>
	{
		private PhoneNumberDatabase _phoneNumberDb;

		public PhoneNumberDatabaseProxy () { }

		private PhoneNumberDatabase loadDb() {
			if (_phoneNumberDb== null) 
			{
				_phoneNumberDb = new PhoneNumberDatabase ();
			}
			return _phoneNumberDb;
		}

		public int DeleteAll()
		{
			return loadDb().DeleteAll ();
		}


		public int DeleteItem(int id)
		{
			return loadDb ().DeleteItem (id);
		}

		public PhoneNumber GetItem(int id)
		{
			return loadDb ().GetItem (id);
		}

		public List<PhoneNumber> GetItems()
		{
			return loadDb ().GetItems ();
		}

		public void SaveItem(PhoneNumber t)
		{
			loadDb ().SaveItem (t);
		}

		public void UpdateItem(PhoneNumber t)
		{
			loadDb ().UpdateItem (t);
		}
	}
}

