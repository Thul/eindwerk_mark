﻿using System;
using Eindwerk.Data_Access;
using Eindwerk.Business.Entities;
using System.Collections.Generic;

namespace Eindwerk.Data_Access.Proxy
{
	/// <summary>
	/// Proxy for the lazy loading of the articledb in the factory
	/// View documentation of ArticleDatabase for an explanation of the different methods
	/// </summary>
	public class ArticleDatabaseProxy : Database<Article>
	{
		private Database<Article> _articleDb;

		public ArticleDatabaseProxy() {}

		private Database<Article> loadDb() {
			if (_articleDb== null) 
			{
				_articleDb= new ArticleDatabase ();
			}
			return _articleDb;
		}

		public int DeleteAll()
		{
			return loadDb().DeleteAll ();
		}


		public int DeleteItem(int id)
		{
			return loadDb ().DeleteItem (id);
		}

		public Article GetItem(int id)
		{
			return loadDb ().GetItem (id);
		}

		public List<Article> GetItems()
		{
			return loadDb ().GetItems ();
		}

		public void SaveItem(Article t)
		{
			loadDb ().SaveItem (t);
		}

		public void UpdateItem(Article t)
		{
			loadDb ().UpdateItem (t);
		}
	}
}

