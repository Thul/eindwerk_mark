﻿using System;
using Eindwerk.Data_Access;
using Eindwerk.Business.Entities;
using System.Collections.Generic;


namespace Eindwerk.Data_Access.Proxy
{
	/// <summary>
	/// Proxy for the lazy loading of the countrydb in the factory
	/// View documentation of CountryDatabase for an explanation of the different methods
	/// </summary>
	public class CountryDatabaseProxy : Database<Country>
	{
		private CountryDatabase _countryDb;

		public CountryDatabaseProxy () { }

		private CountryDatabase loadDb() {
			if (_countryDb== null) 
			{
				_countryDb = new CountryDatabase ();
			}
			return _countryDb;
		}

		public int DeleteAll()
		{
			return loadDb().DeleteAll ();
		}


		public int DeleteItem(int id)
		{
			return loadDb ().DeleteItem (id);
		}

		public Country GetItem(int id)
		{
			return loadDb ().GetItem (id);
		}

		public List<Country> GetItems()
		{
			return loadDb ().GetItems ();
		}

		public void SaveItem(Country t)
		{
			loadDb ().SaveItem (t);
		}

		public void UpdateItem(Country t)
		{
			loadDb ().UpdateItem (t);
		}
	}
}

