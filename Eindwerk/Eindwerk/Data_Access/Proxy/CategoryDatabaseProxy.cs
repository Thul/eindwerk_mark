﻿using System;
using Eindwerk.Business.Entities;
using System.Collections.Generic;

namespace Eindwerk.Data_Access.Proxy
{
	/// <summary>
	/// Proxy for the lazy loading of the categorydb in the factory
	/// View documentation of CategoryDatabase for an explanation of the different methods
	/// </summary>
	public class CategoryDatabaseProxy:Database<Category>
	{
		private CategoryDatabase _categoryDb;

		public CategoryDatabaseProxy () { }

		private CategoryDatabase loadDb() {
			if (_categoryDb== null) 
			{
				_categoryDb = new CategoryDatabase ();
			}
			return _categoryDb;
		}

		public int DeleteAll()
		{
			return loadDb().DeleteAll ();
		}


		public int DeleteItem(int id)
		{
			return loadDb ().DeleteItem (id);
		}

		public Category GetItem(int id)
		{
			return loadDb ().GetItem (id);
		}

		public List<Category> GetItems()
		{
			return loadDb ().GetItems ();
		}

		public void SaveItem(Category t)
		{
			loadDb ().SaveItem (t);
		}

		public void UpdateItem(Category t)
		{
			loadDb ().UpdateItem (t);
		}
	}
}

