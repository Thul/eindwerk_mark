﻿using System;
using Eindwerk.Business.Entities;
using Eindwerk.Data_Access;
using System.Collections.Generic;

namespace Eindwerk.Data_Access.Proxy 
{
	/// <summary>
	/// Proxy for the lazy loading of the tagdb in the factory
	/// View documentation of TagDatabase for an explanation of the different methods
	/// </summary>
	public class TagDatabaseProxy : Database<Tag>
	{
		private TagDatabase _tagDb;

		public TagDatabaseProxy () { }

		private TagDatabase loadDb() {
			if (_tagDb== null) 
			{
				_tagDb = new TagDatabase ();
			}
			return _tagDb;
		}

		public int DeleteAll()
		{
			return loadDb().DeleteAll ();
		}


		public int DeleteItem(int id)
		{
			return loadDb ().DeleteItem (id);
		}

		public Tag GetItem(int id)
		{
			return loadDb ().GetItem (id);
		}

		public List<Tag> GetItems()
		{
			return loadDb ().GetItems ();
		}

		public void SaveItem(Tag t)
		{
			loadDb ().SaveItem (t);
		}

		public void UpdateItem(Tag t)
		{
			loadDb ().UpdateItem (t);
		}
	}
}

