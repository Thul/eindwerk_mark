﻿using Eindwerk.View;
using Eindwerk.View.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eindwerk.Data;
using Xamarin.Forms;
using Eindwerk.Business.Logic;
using Plugin.Messaging;
using Eindwerk.View.Menu;

namespace Eindwerk
{
	public class App : Application
	{

		public static Size ScreenSize { get; set; }

		public App ()
		{
			DbManagerTemplate dbManager = new DbManager ();
			dbManager.DeleteAndCreate ();

            //MainPage = new NavigationPage (new Temp_MainTiles()/*new ThreeTierMenu (new ThreeTierTester().PrimaryMenuItems)*/);

            //MainPage = new NavigationPage (new ThreeTierMenu (new ThreeTierTester().MenuItems)); /*new Temp_MainTiles2()*/

			// MainPage = new NavigationPage (new Eindwerk.View.Menu.Main());

			//MainPage = new HowToReanimate ();

			MainPage = new NavigationPage (new Eindwerk.View.Menu.Main());


			MainPage.ToolbarItems.Add(new ToolbarItem("Noodnummer 112 bellen", "appbar_phone1.png", async() =>
				{
                        var phoneCallTask = MessagingPlugin.PhoneDialer;
                        if (phoneCallTask.CanMakePhoneCall)
                            phoneCallTask.MakePhoneCall("112");
                }));
		}

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
