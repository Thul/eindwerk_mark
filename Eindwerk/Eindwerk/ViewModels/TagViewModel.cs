﻿using System;

namespace Eindwerk.ViewModels
{
	/// <summary>
	/// Viewmodel for tag entity class
	/// </summary>
	public class TagViewModel
	{
		public string Name { get; set; }

		public TagViewModel ()
		{
		}
	}
}

