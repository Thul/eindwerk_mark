﻿using System;
using Eindwerk.Business.Entities;
using Eindwerk.Business.Logic.Service;
using System.Collections.Generic;
using System.Linq;

namespace Eindwerk.ViewModels
{
	/// <summary>
	/// Viewmodel for category entity class
	/// </summary>
	public class CategoryViewModel
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public IEnumerable<ArticleViewModel> Articles;
		public string ImagePath { get; set; }
		public string Color { get; set; }
		public IEnumerable<CategoryViewModel> Categories;

		public CategoryViewModel() { }

		/// <summary>
		/// Create viewmodel, populate with articleviewmodels 
		/// and child categories
		/// </summary>
		/// <param name="categoryId">Corresponding id of category in db</param>
		public CategoryViewModel (int categoryId)
		{
			Articles = new List<ArticleViewModel> ();

			Category category = ServiceLocator.GetInstance ().GetCategoryService ().getItem (categoryId);
			Id = category.Id;
			Name = category.Name;
			ImagePath = category.ImagePath;
			Color = category.Color;
			foreach (Article art in category.Articles) {
				Articles = Articles.Concat (new[]{ new ArticleViewModel (art.Id) });
			}
			Articles = Articles.Reverse ();
			Categories = new List<CategoryViewModel> ();
			if (category.Parent) {
				IEnumerable<Category> cats = ServiceLocator.GetInstance ().GetCategoryService ().GetItems ().Where ((x) => x.ParentId == category.Id);

				foreach (Category c in cats) {
					Categories = Categories.Concat(new[]{new CategoryViewModel(c.Id)});
				}
			}

		}
	}
}

