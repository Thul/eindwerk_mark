﻿using System;
using Eindwerk.Business.Logic.Service;
using Eindwerk.Business.Entities;
using System.Collections.Generic;
using System.Linq;

namespace Eindwerk.ViewModels
{
	/// <summary>
	/// Viewmodel for province entity class
	/// </summary>
	public class ProvinceViewModel
	{
		public int Id {get; set;}
		public string Name {get; set;}
		public string CountryCode { get; set; }
		public IEnumerable<PhoneNumberViewModel> PhoneNumbers { get; set; }

		/// <summary>
		/// Create viewmodel
		/// </summary>
		/// <param name="provinceId">Corresponding id of province in db</param>
		public ProvinceViewModel (int provinceId)
		{
			PhoneNumbers = new List<PhoneNumberViewModel> ();
			Province province = ServiceLocator.GetInstance ().GetProvinceService ().getItem (provinceId);
			Id = province.Id;
			Name = province.Name;
			CountryCode = province.CountryCode;
			PhoneNumbers = ServiceLocator.GetInstance ().GetPhoneNumberService ().GetItems ().Where ((x) => x.ProvinceId == provinceId).Select ((x) => new PhoneNumberViewModel{ Name=x.Name, Number=x.Number });
		}
	}
}

