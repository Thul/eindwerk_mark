﻿using System;
using System.Collections.Generic;
using Eindwerk.Business.Entities;
using Eindwerk.Business.Logic.Service;
using System.Linq;

namespace Eindwerk.ViewModels
{
	/// <summary>
	/// Main entry point for all viewmodels
	/// </summary>
	public class MainViewModel
	{

		public ArticleViewModel Article { get; }
		public IEnumerable<CountryViewModel> Countries { get; set; }
		public IEnumerable<ArticleViewModel> Articles { get; set; }

		/// <summary>
		/// Create mainviewmodel, populate with existing article to bind on
		/// </summary>
		/// <param name="articleId">Corresponding id of article in db</param>
		public MainViewModel (int articleId)
		{
			Article = new ArticleViewModel (articleId);
		}

		/// <summary>
		/// Create mainviewmodel without populating with existing article to bind on
		/// </summary>
		public MainViewModel() {}

		/// <summary>
		/// Get list of all countries in the db in the form of its viewmodel
		/// </summary>
		/// <returns>IEnumerable of countryviewmodels</returns>
		public IEnumerable<CountryViewModel> GetCountryViewModels () {
			if (Countries == null) {
				Countries = new List<CountryViewModel> ();
				List<Country> countries = (List<Country>)ServiceLocator.GetInstance ().GetCountryService ().GetItems ();
				foreach (Country c in countries) {
					Countries = Countries.Concat(new[] { new CountryViewModel(c.Id)});
				}
			}

			return Countries;
		}

		/// <summary>
		/// Get list of countries. Don't get heavy viewmodels, just get basic information
		/// </summary>
		/// <returns>Dictionary of id's as key and country names as value</returns>
		public Dictionary<string, string> GetCountries() {
			Dictionary<string, string> result = new Dictionary<string, string> ();
			IEnumerable<Country> countries = ServiceLocator.GetInstance ().GetCountryService ().GetItems ();
			foreach (Country c in countries) {
				result.Add (c.CountryCode, c.Name);
			}
			return result;
		}

		/// <summary>
		/// Get list of provinces. Don't get heavy viewmodels, just get basic information
		/// </summary>
		/// <returns>IEnumerable of province names</returns>
		public IEnumerable<string> GetProvinces(string countryCode) {
			IEnumerable<Province> provinces = ServiceLocator.GetInstance ().GetProvinceService ().GetItems ();
			return provinces.Where ((x) => x.CountryCode == countryCode).Select ((x) => (x.Id + 1) + " - " + x.Name);
		}

		/// <summary>
		/// Get all main categories with it's child category viewmodels and articleviewmodels
		/// </summary>
		/// <returns>IEnumerable of all main categories</returns>
		public IEnumerable<CategoryViewModel> GetCategoryViewModels () {
			ServiceLocator locator = ServiceLocator.GetInstance ();
			IEnumerable<CategoryViewModel> result = new List<CategoryViewModel> ();
			IEnumerable<Category> categories = locator.GetCategoryService ().GetItems ().Where ((x) => x.Parent == true);
			foreach (Category c in categories) {
				result = result.Concat (new []{ new CategoryViewModel(c.Id) });
			}
			return result.Reverse ();
		}

		public IEnumerable<ArticleViewModel> GetArticles() {
			ServiceLocator locator = ServiceLocator.GetInstance ();
			IEnumerable<ArticleViewModel> result = new List<ArticleViewModel> ();
			IEnumerable<Article> articles = locator.GetArticleService().GetItems().GroupBy(p => p.Title).Select(g => g.First());
			foreach (Article a in articles) {
				result = result.Concat (new []{ new ArticleViewModel(a.Id) });
			}
			return result.Reverse ();
		}

	}
}

