﻿using System;
using Eindwerk.Business.Entities;
using Eindwerk.Business.Logic.Service;

namespace Eindwerk.ViewModels
{
	/// <summary>
	/// Viewmodel for PhoneNumber entity class
	/// </summary>
	public class PhoneNumberViewModel
	{
		public string Name { get; set; }
		public string Number { get; set; }

		public PhoneNumberViewModel ()
		{
			
		}
	}
}

