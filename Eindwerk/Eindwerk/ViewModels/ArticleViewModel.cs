﻿using System;
using System.Collections.Generic;
using Eindwerk.ViewModels;
using Eindwerk.Business.Entities;
using Eindwerk.Business.Logic.Service;
using System.Linq;
namespace Eindwerk.ViewModels
{
	/// <summary>
	/// Viewmodel for article entity class
	/// </summary>
	public class ArticleViewModel:IViewModel
	{
		public int Id { get; set; }
		public string Title { get; set; }
		public IEnumerable<TagViewModel> Tags { get; set; }
		public string Color { get; set; }
		public string Image { get; set;}

		/// <summary>
		/// Create viewmodel, populate with tagviewmodels
		/// </summary>
		/// <param name="pageId">Corresponding id of article in db</param>
		public ArticleViewModel (int pageId)
		{
			Tags = new List<TagViewModel> ();
			Article article = ServiceLocator.GetInstance ().GetArticleService ().getItem (pageId);
			Title = article.Title;
			foreach (Tag t in article.Tags) {
				Tags = Tags.Concat(new[] { new TagViewModel {
						Name = t.Name
					}});
			}
			Color = article.Color;
			Image = article.Image;
			Id = article.Id;

		}
	}
}

