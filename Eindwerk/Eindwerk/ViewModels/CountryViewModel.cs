﻿using System;
using System.Collections.Generic;
using Eindwerk.Business.Entities;
using Eindwerk.Business.Logic.Service;
using System.Linq;
namespace Eindwerk.ViewModels
{
	/// <summary>
	/// Viewmodel for country entity class
	/// </summary>
	public class CountryViewModel : IViewModel
	{
		public int Id {get; set;} 
		public string Name { get; set; }
		public IEnumerable<PhoneNumberViewModel> PhoneNumbers;
		public IEnumerable<ProvinceViewModel> Provinces;
		public string CountryCode {get;set;}

		public CountryViewModel() { }

		/// <summary>
		/// Create viewmodel
		/// </summary>
		/// <param name="countryId">Corresponding id of country in db</param>
		public CountryViewModel (int countryId)
		{
			Country country = ServiceLocator.GetInstance ().GetCountryService ().getItem (countryId);
			Name = country.Name;
			PhoneNumbers = ServiceLocator.GetInstance ().GetPhoneNumberService ().GetItems ().Where ((x) => x.CountryCode == country.CountryCode).Select ((x) => new PhoneNumberViewModel{ Name=x.Name, Number=x.Number });;
			Provinces = ServiceLocator.GetInstance ().GetProvinceService ().GetItems ().Where ((x) => x.CountryCode == country.CountryCode).Select ((x) => new ProvinceViewModel(x.Id));
			CountryCode = country.CountryCode;
		}	
	}
}

