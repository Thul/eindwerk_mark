﻿using System;
using Eindwerk.Data_Access;
using Eindwerk.Business.Entities;
using System.Collections.Generic;

namespace Eindwerk.Data
{
	/// <summary>
	/// Initializes, truncates, seeds database
	/// Settings database
	/// </summary>
	public class DbManager : DbManagerTemplate
	{

		private Database<Article> _articleDb;
		private Database<Country> _countryDb;
		private Database<PhoneNumber> _phoneNumberDb;
		private Database<Province> _provinceDb;
		private Database<Tag> _tagDb;
		private Database<Category> _categoryDb;
		public static readonly string DB_LOCATION = "SQLiteEindwerk.db3";

		/// <summary>
		/// Create all database proxies
		/// </summary>
		public DbManager ()
		{
			_articleDb = DatabaseFactory<Article>.CreateDatabase ();
			_countryDb = DatabaseFactory<Country>.CreateDatabase ();
			_phoneNumberDb = DatabaseFactory<PhoneNumber>.CreateDatabase ();
			_provinceDb = DatabaseFactory<Province>.CreateDatabase ();
			_tagDb = DatabaseFactory<Tag>.CreateDatabase ();
			_categoryDb = DatabaseFactory<Category>.CreateDatabase ();
		}

		/// <summary>
		/// Returns wether all tables have 0 rows
		/// </summary>
		/// <returns>Boolean empty/not empty</returns>
		protected override bool IsDbEmpty ()
		{
			return _articleDb.GetItems ().Count == 0 && _countryDb.GetItems ().Count == 0 && _phoneNumberDb.GetItems ().Count == 0 && _provinceDb.GetItems ().Count == 0 && _tagDb.GetItems ().Count == 0 && _categoryDb.GetItems().Count == 0;
		}

		/// <summary>
		/// Deletes all rows from the database
		/// </summary>
		protected override void Truncate() {
			_articleDb.DeleteAll ();
			_countryDb.DeleteAll ();
			_phoneNumberDb.DeleteAll ();
			_provinceDb.DeleteAll ();
			_tagDb.DeleteAll ();
			_categoryDb.DeleteAll ();
		}

		/// <summary>
		/// Seed database with initial data
		/// </summary>
		protected override void Seed() {
			///Create tags for every article
			int tagCount = 0;
			Tag tagGlasgow1 = new Tag { Id=++tagCount, Name="reactie", PageId=2 };
			Tag tagGlasgow2 = new Tag { Id=++tagCount, Name="beweging", PageId=2 };
			Tag tagGlasgow3 = new Tag { Id=++tagCount, Name="bewustzijn", PageId=2 };
			_tagDb.SaveItem (tagGlasgow1);
			_tagDb.SaveItem (tagGlasgow2);
			_tagDb.SaveItem (tagGlasgow3);

			Tag tagOxigen1 = new Tag { Id=++tagCount, Name = "O₂", PageId=5 };
			Tag tagOxigen2 = new Tag { Id=++tagCount, Name = "O2", PageId=5 };
			Tag tagOxigen3 = new Tag { Id=++tagCount, Name = "toediening", PageId=5 };
			Tag tagOxigen4 = new Tag { Id=++tagCount, Name = "ademen", PageId=5 };
			Tag tagOxigen5 = new Tag { Id=++tagCount, Name = "ademhaling", PageId=5 };
			_tagDb.SaveItem (tagOxigen1);
			_tagDb.SaveItem (tagOxigen2);
			_tagDb.SaveItem (tagOxigen3);
			_tagDb.SaveItem (tagOxigen4);
			_tagDb.SaveItem (tagOxigen5);

			Tag tagHartfailure1 = new Tag { Id=++tagCount, Name = "accuut", PageId=4 };
			Tag tagHartfailure2 = new Tag { Id=++tagCount, Name = "borststreek", PageId=4 };
			Tag tagHartfailure3 = new Tag { Id=++tagCount, Name = "bovenlichaam", PageId=4 };
			Tag tagHartfailure4 = new Tag { Id=++tagCount, Name = "hartaanval", PageId=4 };
			_tagDb.SaveItem (tagHartfailure1);
			_tagDb.SaveItem (tagHartfailure2);
			_tagDb.SaveItem (tagHartfailure3);
			_tagDb.SaveItem (tagHartfailure4);

			Tag tagRecoveryPosition1 = new Tag { Id=++tagCount, Name = "liggen", PageId=12 };
			Tag tagRecoveryPosition2 = new Tag { Id=++tagCount, Name = "stabiel", PageId=12 };
			Tag tagRecoveryPosition3 = new Tag { Id=++tagCount, Name = "ongeval", PageId=12 };
			Tag tagRecoveryPosition4 = new Tag { Id=++tagCount, Name = "positie", PageId=12 };
			Tag tagRecoveryPosition5 = new Tag { Id=++tagCount, Name = "vallen", PageId=12 };
			Tag tagRecoveryPosition6 = new Tag { Id=++tagCount, Name = "grond", PageId=12 };
			_tagDb.SaveItem (tagRecoveryPosition1);
			_tagDb.SaveItem (tagRecoveryPosition2);
			_tagDb.SaveItem (tagRecoveryPosition3);
			_tagDb.SaveItem (tagRecoveryPosition4);
			_tagDb.SaveItem (tagRecoveryPosition5);
			_tagDb.SaveItem (tagRecoveryPosition6);

			Tag tagMonitoringAlarmwaardes1 = new Tag { Id=++tagCount, Name = "meten", PageId=1 };
			Tag tagMonitoringAlarmwaardes2 = new Tag { Id=++tagCount, Name = "meting", PageId=1 };
			_tagDb.SaveItem (tagMonitoringAlarmwaardes1);
			_tagDb.SaveItem (tagMonitoringAlarmwaardes2);

			Tag tagBrainInjury1 = new Tag { Id=++tagCount, Name = "hersenen", PageId=22 };
			Tag tagBrainInjury2 = new Tag { Id=++tagCount, Name = "hoofd", PageId=22 };
			Tag tagBrainInjury3 = new Tag { Id=++tagCount, Name = "letsel", PageId=22 };
			Tag tagBrainInjury4 = new Tag { Id=++tagCount, Name = "ongeval", PageId=22 };
			Tag tagBrainInjury5 = new Tag { Id=++tagCount, Name = "verwonding", PageId=22 };
			Tag tagBrainInjury6 = new Tag { Id=++tagCount, Name = "cerebraal", PageId=22 };
			_tagDb.SaveItem (tagBrainInjury1);
			_tagDb.SaveItem (tagBrainInjury2);
			_tagDb.SaveItem (tagBrainInjury3);
			_tagDb.SaveItem (tagBrainInjury4);
			_tagDb.SaveItem (tagBrainInjury5);
			_tagDb.SaveItem (tagBrainInjury6);

			Tag tagBurning1 = new Tag { Id=++tagCount, Name = "graads", PageId= 18};
			Tag tagBurning2 = new Tag { Id=++tagCount, Name = "brandwonde", PageId= 18 };
			Tag tagBurning3 = new Tag { Id=++tagCount, Name = "verbranden", PageId= 18 };
			Tag tagBurning4 = new Tag { Id=++tagCount, Name = "vuur", PageId= 18 };
			Tag tagBurning5 = new Tag { Id=++tagCount, Name = "letsel", PageId= 18 };
			Tag tagBurning6 = new Tag { Id=++tagCount, Name = "verwonding", PageId= 18 };
			Tag tagBurning7 = new Tag { Id=++tagCount, Name = "ongeval", PageId= 18 };
			_tagDb.SaveItem (tagBurning1);
			_tagDb.SaveItem (tagBurning2);
			_tagDb.SaveItem (tagBurning3);
			_tagDb.SaveItem (tagBurning4);
			_tagDb.SaveItem (tagBurning5);
			_tagDb.SaveItem (tagBurning6);
			_tagDb.SaveItem (tagBurning7);

			Tag tagToxidrome1 = new Tag { Id=++tagCount, Name = "vergiftiging", PageId= 17  };
			Tag tagToxidrome2 = new Tag { Id=++tagCount, Name = "serotonine syndroom", PageId= 17 };
			Tag tagToxidrome3 = new Tag { Id=++tagCount, Name = "anticholinerg", PageId= 17 };
			Tag tagToxidrome4 = new Tag { Id=++tagCount, Name = "cholinerg", PageId= 17 };
			Tag tagToxidrome5 = new Tag { Id=++tagCount, Name = "sympathomimetisch", PageId= 17 };
			Tag tagToxidrome6 = new Tag { Id=++tagCount, Name = "opiaat", PageId= 17 };
			Tag tagToxidrome7 = new Tag { Id=++tagCount, Name = "sedatief", PageId= 17 };
			Tag tagToxidrome8 = new Tag { Id=++tagCount, Name = "alcohol", PageId= 17 };
			Tag tagToxidrome9 = new Tag { Id=++tagCount, Name = "overdosis", PageId= 17 };
			_tagDb.SaveItem (tagToxidrome1);
			_tagDb.SaveItem (tagToxidrome2);
			_tagDb.SaveItem (tagToxidrome3);
			_tagDb.SaveItem (tagToxidrome4);
			_tagDb.SaveItem (tagToxidrome5);
			_tagDb.SaveItem (tagToxidrome6);
			_tagDb.SaveItem (tagToxidrome7);
			_tagDb.SaveItem (tagToxidrome8);
			_tagDb.SaveItem (tagToxidrome9);

			Tag tagRithmPumpingAdults1 = new Tag { Id=++tagCount, Name = "hartritme", PageId= 8 };
			Tag tagRithmPumpingAdults2 = new Tag { Id=++tagCount, Name = "hart", PageId= 8  };
			Tag tagRithmPumpingAdults3 = new Tag { Id=++tagCount, Name = "reanimatie", PageId= 8  };
			Tag tagRithmPumpingAdults4 = new Tag { Id=++tagCount, Name = "geluid", PageId= 8  };
			Tag tagRithmPumpingAdults5 = new Tag { Id=++tagCount, Name = "toon", PageId= 8 };
			_tagDb.SaveItem (tagRithmPumpingAdults1);
			_tagDb.SaveItem (tagRithmPumpingAdults2);
			_tagDb.SaveItem (tagRithmPumpingAdults3);
			_tagDb.SaveItem (tagRithmPumpingAdults4);
			_tagDb.SaveItem (tagRithmPumpingAdults5);

			Tag tagRithmPumpingChildren1 = new Tag { Id=++tagCount, Name = "hartritme", PageId= 13 };
			Tag tagRithmPumpingChildren2 = new Tag { Id=++tagCount, Name = "hart", PageId= 13 };
			Tag tagRithmPumpingChildren3 = new Tag { Id=++tagCount, Name = "reanimatie", PageId= 13 };
			Tag tagRithmPumpingChildren4 = new Tag { Id=++tagCount, Name = "geluid", PageId= 13 };
			Tag tagRithmPumpingChildren5 = new Tag { Id=++tagCount, Name = "toon", PageId= 13 };
			_tagDb.SaveItem (tagRithmPumpingChildren1);
			_tagDb.SaveItem (tagRithmPumpingChildren2);
			_tagDb.SaveItem (tagRithmPumpingChildren3);
			_tagDb.SaveItem (tagRithmPumpingChildren4);
			_tagDb.SaveItem (tagRithmPumpingChildren5);

			Tag tagMedicatieReanimatie1 = new Tag { Id=++tagCount, Name = "dosis", PageId= 11 };
			Tag tagMedicatieReanimatie2 = new Tag { Id=++tagCount, Name = "medicijnen", PageId= 11 };
			Tag tagMedicatieReanimatie3 = new Tag { Id=++tagCount, Name = "Adrenaline", PageId= 11 };
			Tag tagMedicatieReanimatie4 = new Tag { Id=++tagCount, Name = "Amiodarone", PageId= 11 };
			_tagDb.SaveItem (tagMedicatieReanimatie1);
			_tagDb.SaveItem (tagMedicatieReanimatie2);
			_tagDb.SaveItem (tagMedicatieReanimatie3);
			_tagDb.SaveItem (tagMedicatieReanimatie4);

			Tag tagAnaphylaxis1 = new Tag { Id=++tagCount, Name = "anaphylactisch", PageId= 21};
			Tag tagAnaphylaxis2 = new Tag { Id=++tagCount, Name = "shock", PageId= 21 };
			Tag tagAnaphylaxis3 = new Tag { Id=++tagCount, Name = "electief", PageId= 21 };
			Tag tagAnaphylaxis4 = new Tag { Id=++tagCount, Name = "resipartoire", PageId= 21 };
			Tag tagAnaphylaxis5 = new Tag { Id=++tagCount, Name = "larynxoedeem", PageId= 21 };
			Tag tagAnaphylaxis6 = new Tag { Id=++tagCount, Name = "bronchospasme", PageId= 21 };
			_tagDb.SaveItem (tagAnaphylaxis1);
			_tagDb.SaveItem (tagAnaphylaxis2);
			_tagDb.SaveItem (tagAnaphylaxis3);
			_tagDb.SaveItem (tagAnaphylaxis4);
			_tagDb.SaveItem (tagAnaphylaxis5);
			_tagDb.SaveItem (tagAnaphylaxis6);

			Tag tagReanimatieDiagram1 = new Tag { Id=++tagCount, Name = "hartmassage", PageId= 9 };
			Tag tagReanimatieDiagram2 = new Tag { Id=++tagCount, Name = "hart", PageId= 9 };
			_tagDb.SaveItem (tagReanimatieDiagram1);
			_tagDb.SaveItem (tagReanimatieDiagram2);

			Tag tag2ReanimatieDiagram1 = new Tag { Id=++tagCount, Name = "hartmassage", PageId=14 };
			Tag tag2ReanimatieDiagram2 = new Tag { Id=++tagCount, Name = "hart", PageId=14 };
			_tagDb.SaveItem (tag2ReanimatieDiagram1);
			_tagDb.SaveItem (tag2ReanimatieDiagram2);

			Tag tagBevallingen1 = new Tag { Id=++tagCount, Name = "kinderen", PageId=20 };
			Tag tagBevallingen2 = new Tag { Id=++tagCount, Name = "bevallen", PageId=20 };
			Tag tagBevallingen3 = new Tag { Id=++tagCount, Name = "stuitligging", PageId=20 };
			_tagDb.SaveItem (tagBevallingen1);
			_tagDb.SaveItem (tagBevallingen2);
			_tagDb.SaveItem (tagBevallingen3);

			Tag tagAVPU1 = new Tag { Id=++tagCount, Name = "reactie", PageId=3 };
			Tag tagAVPU2 = new Tag { Id=++tagCount, Name = "bewustzijn", PageId=3 };
			_tagDb.SaveItem (tagAVPU1);
			_tagDb.SaveItem (tagAVPU2);

			Tag tagChestPain1 = new Tag { Id=++tagCount, Name = "hart", PageId=19 };
			Tag tagChestPain2 = new Tag { Id=++tagCount, Name = "borstkas", PageId=19 };
			Tag tagChestPain3 = new Tag { Id=++tagCount, Name = "bovenlichaam", PageId=19 };
			_tagDb.SaveItem (tagChestPain1);
			_tagDb.SaveItem (tagChestPain2);
			_tagDb.SaveItem (tagChestPain3);

			Tag tagPhoneNumbers1 = new Tag { Id=++tagCount, Name = "bellen", PageId=23 };
			Tag tagPhoneNumbers2 = new Tag { Id=++tagCount, Name = "alarmnummers", PageId=23 };
			_tagDb.SaveItem (tagPhoneNumbers1);
			_tagDb.SaveItem (tagPhoneNumbers2);

			Tag tagElectrodes1 = new Tag { Id=++tagCount, Name = "ECG", PageId=7 };
			Tag tagElectrodes2 = new Tag { Id=++tagCount, Name = "scan", PageId=7 };
			_tagDb.SaveItem (tagElectrodes1);
			_tagDb.SaveItem (tagElectrodes2);

			Tag tagBewustzijnsverlies1 = new Tag { Id=++tagCount, Name = "flauw vallen", PageId=6 };
			Tag tagBewustzijnsverlies2 = new Tag { Id=++tagCount, Name = "coma", PageId=6 };
			_tagDb.SaveItem (tagBewustzijnsverlies1);
			_tagDb.SaveItem (tagBewustzijnsverlies2);

			Tag tagHartMassageFilm1 = new Tag { Id=++tagCount, Name = "filmpje", PageId=10 };
			Tag tagHartMassageFilm2 = new Tag { Id=++tagCount, Name = "beeld", PageId=10 };
			Tag tagHartMassageFilm3 = new Tag { Id=++tagCount, Name = "reanimatie", PageId=10 };
			_tagDb.SaveItem (tagHartMassageFilm1);
			_tagDb.SaveItem (tagHartMassageFilm2);
			_tagDb.SaveItem (tagHartMassageFilm3);

			Tag tag2HartMassageFilm1 = new Tag { Id=++tagCount, Name = "filmpje", PageId=15 };
			Tag tag2HartMassageFilm2 = new Tag { Id=++tagCount, Name = "beeld", PageId=15 };
			Tag tag2HartMassageFilm3 = new Tag { Id=++tagCount, Name = "reanimatie", PageId=15 };
			_tagDb.SaveItem (tag2HartMassageFilm1);
			_tagDb.SaveItem (tag2HartMassageFilm2);
			_tagDb.SaveItem (tag2HartMassageFilm3);

			Tag tagAfkapwaardes1 = new Tag { Id=++tagCount, Name = "o₂", PageId=16 };
			Tag tagAfkapwaardes2 = new Tag { Id=++tagCount, Name = "O2", PageId=16 };
			Tag tagAfkapwaardes3 = new Tag { Id=++tagCount, Name = "ademhaling", PageId=16 };
			Tag tagAfkapwaardes4 = new Tag { Id=++tagCount, Name = "hartritme", PageId=16 };
			Tag tagAfkapwaardes5 = new Tag { Id=++tagCount, Name = "SpO2", PageId=16 };
			_tagDb.SaveItem (tagAfkapwaardes1);
			_tagDb.SaveItem (tagAfkapwaardes2);
			_tagDb.SaveItem (tagAfkapwaardes3);
			_tagDb.SaveItem (tagAfkapwaardes4);
			_tagDb.SaveItem (tagAfkapwaardes5);

			Tag tagAboutUs1 = new Tag { Id=++tagCount, Name = "credits", PageId=24 };
			Tag tagAboutUs2 = new Tag { Id=++tagCount, Name = "contact", PageId=24 };
			_tagDb.SaveItem (tagAboutUs1);
			_tagDb.SaveItem (tagAboutUs2);

			Tag Heimlich1 = new Tag { Id=++tagCount, Name = "Heimlich", PageId=25 };
			Tag Heimlich2 = new Tag { Id=++tagCount, Name = "Manoeuvre", PageId=25 };
			_tagDb.SaveItem (Heimlich1);
			_tagDb.SaveItem (Heimlich2);

			/// Create categories
			List<Article> articlesMonitoring = new List<Article>();
			List<Article> articlesReanimatie = new List<Article>();
			List<Article> articlesKinderen = new List<Article>();
			List<Article> articlesKlinischeBeelden = new List<Article>();
			List<Article> articlesPhoneNumbers = new List<Article>();
			List<Article> articlesAboutUs = new List<Article>();
			/// Create subcategories
			List<Article> articlesBewustzijnsschalenSub = new List<Article>();


			/// Create articles and add to list category
			int articleCount = 0;
			Article monitoringAlarmwaardes = new Article { Id = ++articleCount, Title = "Early Warning Signs", Color="80397B", Image="warning.png"};
			Article glassgow = new Article { Id = ++articleCount, Title = "Glassgow schaal", Color="A20025", Image="schaal.png" };
			Article avpu = new Article { Id = ++articleCount, Title = "AVPU schaal", Color="FF8B00", Image="schaal.png" };
			articlesBewustzijnsschalenSub.Add (glassgow);
			articlesBewustzijnsschalenSub.Add (avpu);
			Article hartfalen = new Article { Id = ++articleCount, Title = "Hartfalen", Color="2654A4", Image="hart.png"};
			Article zuurstof = new Article { Id = ++articleCount, Title = "Zuurstof", Color="FF8B00", Image="oxygen.png" };
			Article bewustzijnsverlies = new Article { Id = ++articleCount, Title = "Bewustzijns-verlies", Color="EA1E63", Image="pulse.png"};
			Article electroden = new Article { Id = ++articleCount, Title = "Electroden picco monitor", Color="008C00", Image="persoon.png"};
			articlesMonitoring.Add (monitoringAlarmwaardes);
			articlesMonitoring.Add (hartfalen);
			articlesMonitoring.Add (zuurstof);
			articlesMonitoring.Add (bewustzijnsverlies);
			articlesMonitoring.Add (electroden);

			Article ritmeHartAdults = new Article { Id = ++articleCount, Title = "Hartmassage volwassenen", Color="008C00", Image="hart.png"  };
			Article reanimationDiagram = new Article { Id = ++articleCount, Title = "Reanimatie diagram", Color="80397B", Image="diagram.png" };
			Article hearthMassageFilm = new Article { Id = ++articleCount, Title = "Hartmassage film", Color="E76FAD", Image="video.png" };
			Article medicatieReanimatie = new Article { Id = ++articleCount, Title = "Medicatie reanimatie", Color="EA1E63", Image="health.png" };
			Article stabieleZijligging = new Article { Id = ++articleCount, Title = "Stabiele zijligging", Color="2095F2", Image="persoon.png" };
			articlesReanimatie.Add (ritmeHartAdults);
			articlesReanimatie.Add (reanimationDiagram);
			articlesReanimatie.Add (hearthMassageFilm);
			articlesReanimatie.Add (medicatieReanimatie);
			articlesReanimatie.Add (stabieleZijligging);
			//articlesReanimatie.Add (heimlich);

			Article ritmeHartChildren = new Article { Id = ++articleCount, Title = "Hartmassage kinderen", Color="008C00", Image="hart.png" };
			Article reanimationDiagram2 = new Article { Id = ++articleCount, Title = "Reanimatie diagram",  Color="80397B", Image="diagram.png" };
			Article hearthMassageFilm2 = new Article { Id = ++articleCount, Title = "Hartmassage film", Color="E76FAD", Image="video.png" };
			Article afkapwaardes = new Article { Id = ++articleCount, Title = "Afkapwaardes op leeftijd", Color="2095F2", Image="lijst.png" };
			articlesKinderen.Add (ritmeHartChildren);
			articlesKinderen.Add (reanimationDiagram2);
			articlesKinderen.Add (hearthMassageFilm2);
			articlesKinderen.Add (afkapwaardes);

			Article toxidroom = new Article { Id = ++articleCount, Title = "Toxidromen", Color="2654A4", Image="lijst.png" };
			Article verbranding = new Article { Id = ++articleCount, Title = "Verbranding", Color="FF8B00", Image="brand.png" };
			Article borstPijn = new Article{ Id = ++articleCount, Title = "Pijn op de borst",  Color="9B26B2", Image="health.png"  };
			Article bevallingen = new Article { Id = ++articleCount, Title = "Bevallingen",  Color="E76FAD", Image="bevalling.png" };
			Article anafylactischeReactie= new Article { Id = ++articleCount, Title = "Anafylactische reactie", Color="4BAF4F", Image="warning.png" };
			Article brainInjury = new Article { Id = ++articleCount, Title = "Mild Traumatic Brain Injury", Color="A20025", Image="brain.png" };
			articlesKlinischeBeelden.Clear ();
			articlesKlinischeBeelden.Add (toxidroom);
			articlesKlinischeBeelden.Add (verbranding);
			articlesKlinischeBeelden.Add (borstPijn);
			articlesKlinischeBeelden.Add (bevallingen);
			articlesKlinischeBeelden.Add (anafylactischeReactie);
			articlesKlinischeBeelden.Add (brainInjury);

			Article phoneNumberForms = new Article { Id=++articleCount, Title="Noodnummers", Color="FF8B00", Image="pill.png"};
			articlesPhoneNumbers.Add (phoneNumberForms);

			Article aboutUs = new Article { Id=++articleCount, Title="Over ons", Color="FF8B00", Image="pill.png"};
			articlesAboutUs.Add (aboutUs);

			Article heimlich = new Article { Id=++articleCount, Title="Heimlich Manoeuvre", Color="FF8B00", Image="pulse.png"};
			articlesReanimatie.Add (heimlich);
			/// Create parent and sub categories

			int categoryCount = 0;
			Category monitoring = new Category {Id=++categoryCount, Name="Monitoring", Articles = articlesMonitoring, Parent=true, ImagePath="monitoring.png", Color="2095F2"};
			Category bewustzijnsschalen = new Category {Id=++categoryCount, Name="Bewustzijns-schalen", Articles = articlesBewustzijnsschalenSub, Parent=false, ParentId=1, ImagePath="schaal.png", Color="5F7C8A"};
			Category reanimation = new Category {Id=++categoryCount, Name="Reanimatie", Articles = articlesReanimatie, Parent=true, ImagePath="pulse.png", Color="4BAF4F"};
			Category kinderen = new Category {Id=++categoryCount, Name="Kinderen", Articles = articlesKinderen, Parent=true, ImagePath="bevalling.png", Color="9B26B2"};
			Category klinischeBeelden = new Category {Id=++categoryCount, Name="Klinische beelden", Articles = articlesKlinischeBeelden, Parent=true, ImagePath="klinischeBeelden.png", Color="F44236"};
			Category noodnummers = new Category {Id=++categoryCount, Name="Noodnummers", Articles = articlesPhoneNumbers, Parent=true, ImagePath="phone.png", Color="EA1E63"};
			Category overOns = new Category {Id=++categoryCount, Name="Over app", Articles = articlesAboutUs, Parent=true, ImagePath="info.png", Color="5F7C8A"};

			//save cats, tags, articles to db
			_categoryDb.SaveItem(monitoring);
			_categoryDb.SaveItem(bewustzijnsschalen);
			_categoryDb.SaveItem(reanimation);
			_categoryDb.SaveItem(kinderen);
			_categoryDb.SaveItem(klinischeBeelden);
			_categoryDb.SaveItem(noodnummers);
			_categoryDb.SaveItem(overOns);

			///Create phonenumbers
			List<PhoneNumber> belgiePhone = new List<PhoneNumber>();
			List<PhoneNumber> nederlandPhone = new List<PhoneNumber>();
			List<Province> provinces = new List<Province>();

			int phoneNumberCount = _phoneNumberDb.GetItems().Count;
			//Phone numbers Belgium
			PhoneNumber ambulanceBrandweer = new PhoneNumber{Name="Ambulance en brandweer", Number="112", CountryCode="BE", Id=phoneNumberCount++, ProvinceId=-1};
			PhoneNumber politie = new PhoneNumber { Name="Politie", Number="101", CountryCode="BE", Id=phoneNumberCount++, ProvinceId=-1};
			PhoneNumber permanentiedienstApothekers = new PhoneNumber { Name="Permanentiedienst apothekers", Number="900 10 50", CountryCode="BE", Id=phoneNumberCount++, ProvinceId=-1};
			PhoneNumber antigifcentrum = new PhoneNumber { Name="Antigifcentrum", Number="0032 70 245 245", CountryCode="BE", Id=phoneNumberCount++, ProvinceId=-1};
			PhoneNumber childFocus = new PhoneNumber { Name="Child focus", Number="116 000", CountryCode="BE", Id=phoneNumberCount++, ProvinceId=-1};
			PhoneNumber KinderenEnJongerentelefoon = new PhoneNumber { Name="Kinderen en jongerentelefoon", Number="102", CountryCode="BE", Id=phoneNumberCount++, ProvinceId=-1};
			PhoneNumber teleonthaal = new PhoneNumber { Name="Tele-onthaal", Number="106", CountryCode="BE", Id=phoneNumberCount++, ProvinceId=-1};
			PhoneNumber Zelfmoordlijn = new PhoneNumber { Name="Zelfmoordlijn", Number="1813", CountryCode="BE", Id=phoneNumberCount++, ProvinceId=-1};
			PhoneNumber meldpuntGeweld = new PhoneNumber { Name="Meldpunt geweld, misbruik en kindermishandeling", Number="1712", CountryCode="BE", Id=phoneNumberCount++, ProvinceId=-1};
			PhoneNumber brandwondenCentra = new PhoneNumber { Name="Brandwondencentra", Number="00322 268 62 00", CountryCode="BE", Id=phoneNumberCount++, ProvinceId=-1};
			PhoneNumber druglijn = new PhoneNumber { Name="Druglijn", Number="00328 00 96 333", CountryCode="BE", Id=phoneNumberCount++, ProvinceId=-1};

			belgiePhone.Add (ambulanceBrandweer);
			belgiePhone.Add (politie);
			belgiePhone.Add (permanentiedienstApothekers);
			belgiePhone.Add (antigifcentrum);
			belgiePhone.Add (childFocus);
			belgiePhone.Add (KinderenEnJongerentelefoon);
			belgiePhone.Add (teleonthaal);
			belgiePhone.Add (Zelfmoordlijn);
			belgiePhone.Add (meldpuntGeweld);
			belgiePhone.Add (brandwondenCentra);
			belgiePhone.Add (druglijn);

			//Phone numbers Netherlands
			PhoneNumber politieNed = new PhoneNumber{Name="Politie", Number="112", CountryCode="NL", Id=phoneNumberCount++, ProvinceId = -1};
			PhoneNumber ambulanceBrandweerNed = new PhoneNumber{Name="Ambulance en brandweer", Number="112", CountryCode="NL", Id=phoneNumberCount++, ProvinceId = -1};
			PhoneNumber nationaalVergiftigingIC = new PhoneNumber{Name="Nationaal Vergiftigingen Informatie Centrum", Number="0031 302 748 888", CountryCode="NL", Id=phoneNumberCount++, ProvinceId = -1};
			PhoneNumber druglijnNed = new PhoneNumber{Name="Druglijn", Number="0031 78 15 10 20", CountryCode="NL", Id=phoneNumberCount++, ProvinceId = -1};
			PhoneNumber kankerfoon = new PhoneNumber{Name="Kankerfoon", Number="0031 800 15 800", CountryCode="NL", Id=phoneNumberCount++, ProvinceId = -1};
			PhoneNumber kindermishandeling = new PhoneNumber{Name="Advies- en meldpunt Kindermishandeling", Number="0031 900 123 123 0", CountryCode="NL", Id=phoneNumberCount++, ProvinceId = -1};
			PhoneNumber zelfmoordpreventie = new PhoneNumber{Name="Zelfmoordpreventie", Number="0031 900 0113", CountryCode="NL", Id=phoneNumberCount++, ProvinceId = -1};
			PhoneNumber brandwondencentraNed = new PhoneNumber{Name="Brandwondencentra", Number="0031 071 448 000", CountryCode="NL", Id=phoneNumberCount++, ProvinceId = -1};
			PhoneNumber kindertelefoon = new PhoneNumber{Name="Kindertelefoon", Number="103", CountryCode="NL", Id=phoneNumberCount++, ProvinceId = -1};
			PhoneNumber veiligThuis = new PhoneNumber{Name="Veilig Thuis", Number="0031 800 200 0", CountryCode="NL", Id=phoneNumberCount++, ProvinceId = -1};

			nederlandPhone.Add (politieNed);
			nederlandPhone.Add (ambulanceBrandweerNed);
			nederlandPhone.Add (nationaalVergiftigingIC);
			nederlandPhone.Add (druglijnNed);
			nederlandPhone.Add (kankerfoon);
			nederlandPhone.Add (kindermishandeling);
			nederlandPhone.Add (zelfmoordpreventie);
			nederlandPhone.Add (brandwondencentraNed);
			nederlandPhone.Add (kindertelefoon);
			nederlandPhone.Add (veiligThuis);

			//Phone numbers provinces Belgium
			int provinceCount = _provinceDb.GetItems().Count;
			List<Province> provinces_belgium = new List<Province>();

			//Numbers antwerpen
			Province antwerpen = new Province { Id = provinceCount++, CountryCode = "BE", Name = "Antwerpen" }; 
			List<PhoneNumber> phoneNumbersAntwerpen = new List<PhoneNumber> ();
			PhoneNumber vertrouwensCentraAntwerpen = new PhoneNumber {
				Id=phoneNumberCount++, 
				Name="Vertrouwenscentrum Antwerpen",
				CountryCode="BE",
				Number="0032 32 323 041 90",
				ProvinceId=provinceCount-1
			};
			phoneNumbersAntwerpen.Add (vertrouwensCentraAntwerpen);
			antwerpen.PhoneNumbers = phoneNumbersAntwerpen;
			provinces_belgium.Add(antwerpen);

			//Numbers Brussel
			Province brussel = new Province { Id = provinceCount++, CountryCode = "BE", Name = "Brussel" }; 
			List<PhoneNumber> phoneNumbersBrussel = new List<PhoneNumber> ();
			PhoneNumber vertrouwensCentraBrussel = new PhoneNumber {
				Id=phoneNumberCount++, 
				Name="Vertrouwenscentrum Brussel",
				CountryCode="BE",
				Number="0032 247 760 60",
				ProvinceId=provinceCount-1
			};
			phoneNumbersBrussel.Add (vertrouwensCentraBrussel);
			brussel.PhoneNumbers = phoneNumbersBrussel;
			provinces_belgium.Add(brussel);

			//Numbers Limburg
			Province limburg = new Province { Id = provinceCount++, CountryCode = "BE", Name = "Limburg" }; 
			List<PhoneNumber> phoneNumbersLimburg = new List<PhoneNumber> ();
			PhoneNumber vertrouwensCentraLimburg = new PhoneNumber {
				Id=phoneNumberCount++, 
				Name="Vertrouwenscentrum Limburg",
				CountryCode="BE",
				Number="0032 112 746 72",
				ProvinceId=provinceCount-1
			};
			phoneNumbersLimburg.Add (vertrouwensCentraLimburg);
			limburg.PhoneNumbers = phoneNumbersLimburg;
			provinces_belgium.Add(limburg);

			//Numbers Oost-Vlaanderen
			Province oostvl = new Province { Id = provinceCount++, CountryCode = "BE", Name = "Oost-Vlaanderen" }; 
			List<PhoneNumber> phoneNumbersOostvl = new List<PhoneNumber> ();
			PhoneNumber vertrouwensCentraOostvl = new PhoneNumber {
				Id=phoneNumberCount++, 
				Name="Vertrouwenscentrum Oost-Vlaanderen",
				CountryCode="BE",
				Number="0032 921 673 30",
				ProvinceId=provinceCount-1
			};
			phoneNumbersOostvl.Add (vertrouwensCentraOostvl);
			oostvl.PhoneNumbers = phoneNumbersOostvl;
			provinces_belgium.Add(oostvl);

			//Numbers West-Vlaanderen
			Province westvl = new Province { Id = provinceCount++, CountryCode = "BE", Name = "West-Vlaanderen" }; 
			List<PhoneNumber> phoneNumbersWestvl = new List<PhoneNumber> ();
			PhoneNumber vertrouwensCentraWestvl = new PhoneNumber {
				Id=phoneNumberCount++, 
				Name="Vertrouwenscentrum West-Vlaanderen",
				CountryCode="BE",
				ProvinceId=provinceCount-1,
				Number="0032 503 337 08"
			};
			phoneNumbersWestvl.Add (vertrouwensCentraWestvl);
			westvl.PhoneNumbers = phoneNumbersWestvl;
			provinces_belgium.Add(westvl);

			//Numbers West-Vlaanderen
			Province vlaamsBrabant = new Province { Id = provinceCount++, CountryCode = "BE", Name = "Vlaams-Brabant" }; 
			List<PhoneNumber> phoneNumbersVlaamsBrabant = new List<PhoneNumber> ();
			PhoneNumber vertrouwensCentraVlaamsBrabant = new PhoneNumber {
				Id=phoneNumberCount++, 
				Name="Vertrouwenscentrum Vlaams-Brabant",
				CountryCode="BE",
				ProvinceId = provinceCount-1,
				Number="0032 503 337 08"
			};
			phoneNumbersVlaamsBrabant.Add (vertrouwensCentraVlaamsBrabant);
			vlaamsBrabant.PhoneNumbers = phoneNumbersVlaamsBrabant;
			provinces_belgium.Add(vlaamsBrabant);

			//Provinced netherlands
			List<Province> provinces_netherlands = new List<Province>();
			Province drenthe = new Province { Id = provinceCount++, CountryCode = "NL", Name = "Drenthe" }; 
			Province flevoland = new Province { Id = provinceCount++, CountryCode = "NL", Name = "Flevoland" }; 
			Province friesland = new Province { Id = provinceCount++, CountryCode = "NL", Name = "Friesland" }; 
			Province gelderland = new Province { Id = provinceCount++, CountryCode = "NL", Name = "Gelderland" }; 
			Province groningen = new Province { Id = provinceCount++, CountryCode = "NL", Name = "Groningen" }; 
			Province limburgNed = new Province { Id = provinceCount++, CountryCode = "NL", Name = "Limburg" }; 
			Province noordBrabant = new Province { Id = provinceCount++, CountryCode = "NL", Name = "Noord-Brabant" }; 
			Province noordHolland = new Province { Id = provinceCount++, CountryCode = "NL", Name = "Noord-Holland" }; 
			Province overijssel = new Province { Id = provinceCount++, CountryCode = "NL", Name = "Overijssel" }; 
			Province utrecht = new Province { Id = provinceCount++, CountryCode = "NL", Name = "Utrecht" }; 
			Province zeeland = new Province { Id = provinceCount++, CountryCode = "NL", Name = "Zeeland" }; 
			Province zuidHolland = new Province { Id = provinceCount++, CountryCode = "NL", Name = "Zuid-Holland" }; 

			provinces_netherlands.Add (drenthe);
			provinces_netherlands.Add (flevoland);
			provinces_netherlands.Add (friesland);
			provinces_netherlands.Add (gelderland);
			provinces_netherlands.Add (groningen);
			provinces_netherlands.Add (limburgNed);
			provinces_netherlands.Add (noordBrabant);
			provinces_netherlands.Add (noordHolland);
			provinces_netherlands.Add (overijssel);
			provinces_netherlands.Add (utrecht);
			provinces_netherlands.Add (zeeland);
			provinces_netherlands.Add (zuidHolland);

			//Countries
			int countryCount = _countryDb.GetItems().Count;
			Country belgium = new Country {Id=countryCount++, CountryCode="BE", Name="België", PhoneNumbers=belgiePhone, Provinces=provinces_belgium } ;
			Country netherlands = new Country {Id=countryCount++, CountryCode="NL", Name="Nederland", PhoneNumbers=nederlandPhone, Provinces=provinces_netherlands};

			//Save everything
			_phoneNumberDb.SaveItem(vertrouwensCentraAntwerpen);
			_phoneNumberDb.SaveItem(vertrouwensCentraBrussel);
			_phoneNumberDb.SaveItem(vertrouwensCentraLimburg);
			_phoneNumberDb.SaveItem(vertrouwensCentraOostvl);
			_phoneNumberDb.SaveItem(vertrouwensCentraWestvl);
			_phoneNumberDb.SaveItem (vertrouwensCentraVlaamsBrabant);
			_countryDb.SaveItem(belgium);
			_countryDb.SaveItem(netherlands);
		}
			
	}
}

