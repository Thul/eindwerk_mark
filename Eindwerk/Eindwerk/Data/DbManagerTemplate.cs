﻿using System;

namespace Eindwerk.Data
{
	public abstract class DbManagerTemplate
	{
		/// <summary>
		/// Seed database with initial data
		/// </summary>
		protected abstract void Seed();

		/// <summary>
		/// Returns wether all tables have 0 rows
		/// </summary>
		/// <returns>Boolean empty/not empty</returns>
		protected abstract bool IsDbEmpty();

		/// <summary>
		/// Deletes all rows from the database
		/// </summary>
		protected abstract void Truncate();

		/// <summary>
		/// Empty all tables and seed the initial data again
		/// </summary>
		public void DeleteAndCreate() 
		{
			Truncate ();
			Seed ();
		}

		/// <summary>
		/// Seed database with initial data, public
		/// </summary>
		public void Create() 
		{
			Seed ();
		}

		/// <summary>
		/// Seed database if it is not empty
		/// </summary>
		public void CreateIfNotExists() {
			if(IsDbEmpty()) {
				Seed ();
			}
		}
	}
}

