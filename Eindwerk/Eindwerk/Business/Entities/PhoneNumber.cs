﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace Eindwerk.Business.Entities
{
	/// <summary>
	/// PhoneNumber entity class
	/// </summary>
    public class PhoneNumber
    {
        [PrimaryKey]
        public int Id { get; set; }

        public string Name { get; set; }

        public string Number { get; set; }

        [References(typeof(Country))]
        [OnUpdateCascade]
        [OnDeleteCascade]
        public string CountryCode { get; set; }

        [References(typeof(Province))]
        [OnUpdateCascade]
        [OnDeleteCascade]
        public int? ProvinceId { get; set; }


        public PhoneNumber()
        {

        }

		public PhoneNumber(string name, string number, string countryCode)
		{
			Name = name;
			Number = number;
			CountryCode = countryCode;
			ProvinceId = null;
		}

        public PhoneNumber(string name, string number, string countryCode, int provinceId)
        {
            Name = name;
            Number = number;
            CountryCode = countryCode;
            ProvinceId = provinceId;
        }



    }
}
