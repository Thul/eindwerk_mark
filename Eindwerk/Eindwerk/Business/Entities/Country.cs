﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;
using Eindwerk.Business.Entities;

namespace Eindwerk.Business.Entities
{
	/// <summary>
	/// Country entity class
	/// </summary>
    public class Country
    {
        [PrimaryKey]
        public int Id { get; set; }

        public string Name { get; set; }

		public string CountryCode { get; set; }

        [One2Many(typeof(Province))]
        public List<Province> Provinces { get; set; }

        [One2Many(typeof(PhoneNumber))]
        public List<PhoneNumber> PhoneNumbers { get; set; }

        public Country()
        {

        }

        public Country(string name)
        {
            Name = name;
            Provinces = new List<Province>();
            PhoneNumbers = new List<PhoneNumber>();
        }

        public Country(string countryCode, string name, List<Province> provinces, List<PhoneNumber> numbers)
        {
			CountryCode = countryCode;
            Name = name;
            Provinces = provinces;
            PhoneNumbers = numbers;
        }
    }
}
