﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace Eindwerk.Business.Entities
{
	/// <summary>
	/// Article entity class
	/// </summary>
    public class Article
    {
        [PrimaryKey]
        public int Id { get; set; }

        public string Title { get; set; }

		public string Color { get; set; }
		public string Image { get; set; }
	
		[References(typeof(Category))]
		[OnUpdateCascade]
		[OnDeleteCascade]
		public int CategoryId { get; set; }

        [One2Many(typeof(Tag))]
        public List<Tag> Tags { get; set; }

        public Article()
        {

        }

        public Article(string title, List<Tag> tags)
        {
            Title = title;
            Tags = tags;
        }


    }
}
