﻿using System;
using SQLite;
using System.Collections.Generic;
using System.Text;
namespace Eindwerk.Business.Entities
{
	/// <summary>
	/// Category entity class
	/// </summary>
	public class Category
	{
		
		[PrimaryKey]
		public int Id { get; set; }

		public string Name { get; set; }

		public string ImagePath { get; set; }

		public string Color { get; set; }

		public bool Parent { get; set; }

		public int? ParentId { get; set; }

		[One2Many(typeof(Article))]
		public List<Article> Articles { get; set; }

		public Category ()
		{
			
		}

	}
}
