﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace Eindwerk.Business.Entities
{
	/// <summary>
	/// Tag entity class
	/// </summary>
    public class Tag
    {
        [PrimaryKey]
        public int Id { get; set; }
        public string Name { get; set; }


        [References(typeof(Article))]
        [OnUpdateCascade]
        [OnDeleteCascade]
        public int PageId { get; set; }

        public Tag()

        {

        }

        public Tag(string name, int pageId)
        {
            Name = name;
            PageId = pageId;
        }



    }
}
