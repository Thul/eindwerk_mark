﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace Eindwerk.Business.Entities
{
	/// <summary>
	/// Province entity class
	/// </summary>
    public class Province
    {
        [PrimaryKey]
        public int Id { get; set; }

        public string Name { get; set; }

        [One2Many(typeof(PhoneNumber))]
        public List<PhoneNumber> PhoneNumbers { get; set; }

        [References(typeof(Country))]
        [OnUpdateCascade]
        [OnDeleteCascade]
        public string CountryCode { get; set; }

        public Province()
        {

        }

        public Province(string name)
        {
            Name = name;
        }

        public Province(string name, string countryCode, List<PhoneNumber> numbers)
        {
            Name = name;
            PhoneNumbers = numbers;
            CountryCode = countryCode;
        }

    }
}
