﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Eindwerk.Business.Logic
{
	/// <summary>
	/// Interface for services in seperate projects for dialing numbers
	/// </summary>
    public interface IDialer
    {
		/// <summary>
		/// Try dialing a number
		/// </summary>
		/// <returns>Boolean possible</returns>
        bool Dial(string number);
    }
}
