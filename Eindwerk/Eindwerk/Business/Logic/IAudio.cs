﻿using System;

namespace Eindwerk.Business.Logic
{
	/// <summary>
	/// Interface for services in seperate projects for working with audio
	/// </summary>
	public interface IAudio
	{
		/// <summary>
		/// Start playing audio
		/// </summary>
		/// <param name="fileName">Path to the file</param>
		void PlayMp3File(string fileName);

		/// <summary>
		/// Stop playing audio
		/// </summary>
		void StopPlayingFile();
	}
}

