﻿using System;
using Eindwerk.Business.Entities;
using System.Collections.Generic;
using Xamarin.Forms;
using Eindwerk.Data_Access;
using Eindwerk.Data_Access.Proxy;
namespace Eindwerk.Business.Logic.Service
{
	/// <summary>
	/// Generic service for every database
	/// </summary>
	public class DatabaseService<T> : BaseService
	{
		protected Database<T> _db;

		public DatabaseService() {
			_db = DatabaseFactory<T>.CreateDatabase ();	
		}

		/// <summary>
		/// Get entity from the database
		/// </summary>
		/// <returns>Entity T</returns>
		/// <param name="id">id of the entity in the database</param>
		public T getItem (int id)
		{
			return _db.GetItem (id);
		}

		/// <summary>
		/// Get all entities from the database
		/// </summary>
		/// <returns>IEnumberable entity T</returns>
		public IEnumerable<T> GetItems ()
		{
			return _db.GetItems ();
		} 
	}
}

