﻿using System;
using Xamarin.Forms;
using Eindwerk.Business.Logic.Navigation;
using Eindwerk.ViewModels;

namespace Eindwerk.Business.Logic.Service
{
	/// <summary>
	/// Service for the navigation of the app
	/// </summary>
	public class NavigationService:BaseService
	{
		public NavigationService ()	{}

		/// <summary>
		/// Go to specific page
		/// </summary>
		/// <returns>Page which will be displayed</returns>
		/// <param name="pageId">if of the page to navigate to</param>
		public Page NavigateTo(int pageId) {
			MainViewModel vm = new MainViewModel (pageId);
            NavigationCommand navigationCommand = NavigationCommandFactory.CreateCommand(pageId);
            return navigationCommand == null ? null : navigationCommand.Execute (vm); //avoids crash if factory returns null (which happens in the case of wrong page id in database)
		}

	}
}

