﻿using System;
using Eindwerk.Business.Entities;
namespace Eindwerk.Business.Logic.Service
{
	/// <summary>
	/// Locator for all different services, main entry point
	/// </summary>
	public class ServiceLocator
	{
		private DatabaseService<Article> _articleService;
		private DatabaseService<Country> _countryService;
		private DatabaseService<Province> _provinceService;
		private DatabaseService<Category> _categoryService;
		private DatabaseService<PhoneNumber> _phoneNumberService;
		private NavigationService _navigationService;

		/// <summary>
		/// Singleton holder
		/// </summary>
		private static class SingletonHolder {
			public static readonly ServiceLocator instance = new ServiceLocator();
		}

		/// <summary>
		/// Get singleton instance
		/// </summary>
		public static ServiceLocator GetInstance() {
			return SingletonHolder.instance;
		}

		/// <summary>
		/// Lazy load article database service
		/// </summary>
		/// <returns>Database Service for articles</returns>
		public DatabaseService<Article> GetArticleService() {
			if (_articleService == null) {
				_articleService = new DatabaseService<Article> ();
			}
			return _articleService;
		}

		/// <summary>
		/// Lazy load country database service
		/// </summary>
		/// <returns>Database Service for countries</returns>
		public DatabaseService<Country> GetCountryService() {
			if (_countryService == null) {
				_countryService = new DatabaseService<Country> ();
			}
			return _countryService;
		} 

		/// <summary>
		/// Lazy load province database service
		/// </summary>
		/// <returns>Database Service for provinces</returns>
		public DatabaseService<Province> GetProvinceService() {
			if (_provinceService == null) {
				_provinceService = new DatabaseService<Province> ();
			}
			return _provinceService;
		} 

		/// <summary>
		/// Lazy load category database service
		/// </summary>
		/// <returns>Database Service for categories</returns>
		public DatabaseService<Category> GetCategoryService() {
			if (_categoryService == null) {
				_categoryService = new DatabaseService<Category> ();
			}
			return _categoryService;
		} 

		/// <summary>
		/// Lazy load phonenumber database service
		/// </summary>
		/// <returns>Database Service for phonenumbers</returns>
		public DatabaseService<PhoneNumber> GetPhoneNumberService() {
			if (_phoneNumberService == null) {
				_phoneNumberService= new DatabaseService<PhoneNumber> ();
			}
			return _phoneNumberService;
		}

		/// <summary>
		/// Lazy load navigation service
		/// </summary>
		/// <returns>Service for navigating to pages</returns>
		public NavigationService GetNavigationService() {
			if (_navigationService == null) {
				_navigationService = new NavigationService ();
			}
			return _navigationService;
		}


	}
}

