﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;

namespace Eindwerk.Business.Logic.Navigation
{
	public static class NavigationCommandFactory
	{
		private static Dictionary<int, NavigationCommand> _navigationCommands;

		static NavigationCommandFactory() {
			_navigationCommands = new Dictionary<int, NavigationCommand> ();
			_navigationCommands.Add (1, new EarlyWarningSignsCommand ());
			_navigationCommands.Add (2, new GlasgowCommand ());
			_navigationCommands.Add (3, new AVPUCommand());
			_navigationCommands.Add (4, new HartfalenCommand());
            _navigationCommands.Add (5, new ZuurstofToedieningCommand());
            _navigationCommands.Add (6, new BewustzijnsverliesCommand ());
			_navigationCommands.Add (7, new ElectrodesPiccoCommand ());
			_navigationCommands.Add (8, new HartmassageVolwassenenCommand());
			_navigationCommands.Add (9, new DiagramReanimatieCommand());
			_navigationCommands.Add (10, new ReanimationFilmCommand());
			_navigationCommands.Add (11, new MedicatieReanimatieCommand ());
			_navigationCommands.Add (12, new StabieleZijliggingCommand());
			_navigationCommands.Add (13, new HartmassageKinderenCommand());
			_navigationCommands.Add (14, new DiagramReanimatieCommand ());
			_navigationCommands.Add (15, new ReanimationFilmCommand ());
			_navigationCommands.Add (16, new AfkapwaardesCommand ());			
			_navigationCommands.Add (17, new ToxidromenCommand());
			_navigationCommands.Add (18, new VerbrandingenCommand ());
			_navigationCommands.Add (19, new PijnOpBorstCommand());
			_navigationCommands.Add (20, new BevallingenCommand());			
			_navigationCommands.Add (21, new AnaphylactischeReactieCommand());
			_navigationCommands.Add (22, new MTBrainInjuryCommand());
			_navigationCommands.Add (23, new NoodnummersFormulierCommand());
			_navigationCommands.Add (24, new OverAppCommand());
			_navigationCommands.Add (25, new HeimlichCommand ());
		}
			
		public static NavigationCommand CreateCommand(int pageId) 
		{
            if (_navigationCommands.ContainsKey(pageId))
            {
                return _navigationCommands[pageId];
            } else
            {
                return null; //avoids IndexOutOfBounds crash if pageId doesn't exist (returned null will be caught in the main menu class (Main.xaml.cs))
            }
		}
	}
}

