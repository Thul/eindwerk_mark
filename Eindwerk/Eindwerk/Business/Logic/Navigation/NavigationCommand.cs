﻿using System;
using Xamarin.Forms;
using Eindwerk.View.Pages.Extra;
using Eindwerk.View.Pages.Kinderen;
using Eindwerk.View.Pages.KlinischeBeelden;
using Eindwerk.View.Pages.Monitoring;
using Eindwerk.View.Pages.Noodnummers;
using Eindwerk.View.Pages.Reanimatie;
using Eindwerk.ViewModels;

namespace Eindwerk.Business.Logic.Navigation
{
	/// <summary>
	/// Interface navigation commands
	/// </summary>
	public interface NavigationCommand
	{
		/// <summary>
		/// Navigate to page base method
		/// </summary>
		/// <param name="vm">add viewmodel to page</param>
		/// <returns>Page</returns>
		Page Execute (MainViewModel vm);
	}

    /// <summary>
    /// Navigate to Glasgow
    /// </summary>
    public class GlasgowCommand : NavigationCommand
	{
		public Page Execute (MainViewModel viewModel)
		{
			return new Glasgow();
		}
	}
    /// <summary>
    /// Navigate to AVPU
    /// </summary>
    public class AVPUCommand : NavigationCommand
	{
		public Page Execute (MainViewModel viewModel)
		{
			return new AVPU();
		}
	}
    /// <summary>
    /// Navigate to ZuurstofToediening
    /// </summary>
    public class ZuurstofToedieningCommand : NavigationCommand
	{
		public Page Execute (MainViewModel viewModel)
		{
			return new ZuurstofToediening (viewModel);
		}
	}
	/// <summary>
	/// Navigate to Hartfalen
	/// </summary>
	public class HartfalenCommand: NavigationCommand
	{
		public Page Execute (MainViewModel viewModel)
		{
			return new Hartfalen (viewModel);
		}
	}

    /// <summary>
    /// Navigate to StabieleZijligging
    /// </summary>
    public class StabieleZijliggingCommand : NavigationCommand
	{
		public Page Execute (MainViewModel viewModel)
		{
			return new StabieleZijligging (viewModel);
		}
	}

    /// <summary>
    /// Navigate to MTBrainInjury
    /// </summary>
    public class MTBrainInjuryCommand : NavigationCommand
	{
		public Page Execute (MainViewModel viewModel)	{
			return new View.Pages.KlinischeBeelden.MTBrainInjury (viewModel);
		}
	}

    /// <summary>
    /// Navigate to Verbrandingen
    /// </summary>
    public class VerbrandingenCommand : NavigationCommand
	{
		public Page Execute (MainViewModel viewModel)
		{
			return new View.Pages.KlinischeBeelden.Verbrandingen (viewModel);
		}
	}

    /// <summary>
    /// Navigate to Toxidromen
    /// </summary>
    public class ToxidromenCommand: NavigationCommand
	{
		public Page Execute (MainViewModel viewModel)
		{
			return new Toxidromen (viewModel);
		}
	}

    /// <summary>
    /// Navigate to HartmassageVolwassenen
    /// </summary>
    public class HartmassageVolwassenenCommand : NavigationCommand
	{
		public Page Execute (MainViewModel viewModel)
		{
			return new HartmassageVolwassenen (viewModel);
		}
	}

    /// <summary>
    /// Navigate to HartmassageKinderen
    /// </summary>
    public class HartmassageKinderenCommand : NavigationCommand
	{
		public Page Execute (MainViewModel viewModel)
		{
			return new HartmassageKinderen (viewModel);
		}
	}

    /// <summary>
    /// Navigate to MedicatieReanimatie
    /// </summary>
    public class MedicatieReanimatieCommand : NavigationCommand
	{
		public Page Execute (MainViewModel viewModel)
		{
			return new MedicatieReanimatie (viewModel);
		}
	}

    /// <summary>
    /// Navigate to DiagramReanimatie
    /// </summary>
    public class DiagramReanimatieCommand : NavigationCommand
	{
		public Page Execute (MainViewModel viewModel)
		{
			return new DiagramReanimatie (viewModel);
		}
	}

    /// <summary>
    /// Navigate to Bewustzijnsverlies
    /// </summary>
    public class BewustzijnsverliesCommand : NavigationCommand
	{
		public Page Execute (MainViewModel viewModel)
		{
			return new Bewustzijnsverlies (viewModel);
		}
	}

    /// <summary>
    /// Navigate to Bevallingen
    /// </summary>
    public class BevallingenCommand : NavigationCommand
	{
		public Page Execute (MainViewModel viewModel)
		{
			return new Bevallingen (viewModel);
		}
	}

    /// <summary>
    /// Navigate to PijnOpBorst
    /// </summary>
    public class PijnOpBorstCommand : NavigationCommand
	{
		public Page Execute (MainViewModel viewModel)
		{
			return new PijnOpBorst (viewModel);
		}
	}

    /// <summary>
    /// Navigate to AnafylactischeReactie
    /// </summary>
    public class AnaphylactischeReactieCommand : NavigationCommand
	{
		public Page Execute (MainViewModel viewModel)
		{
			return new AnafylactischeReactie (viewModel);
		}
	}

    /// <summary>
    /// Navigate to EarlyWarningSigns
    /// </summary>
    public class EarlyWarningSignsCommand : NavigationCommand
	{
		public Page Execute (MainViewModel viewModel)
		{
			return new EarlyWarningSigns (viewModel);
		}
	}

    /// <summary>
    /// Navigate to NoodnummersFormulier
    /// </summary>
    public class NoodnummersFormulierCommand : NavigationCommand
	{

		public Page Execute (MainViewModel viewModel)
		{
			return new NoodnummersFormulier (viewModel);
		}
	}

    /// <summary>
    /// Navigate to ElectrodesPicco
    /// </summary>
    public class ElectrodesPiccoCommand : NavigationCommand
	{
		public Page Execute (MainViewModel viewModel)
		{
			return new ElectrodesPicco (viewModel);
		}
	}

	/// <summary>
	/// Navigate to FilmpjeReanimatie
	/// </summary>
	public class ReanimationFilmCommand: NavigationCommand
	{
		public Page Execute (MainViewModel viewModel)
		{
			return null; //to-do
		}
	}

    /// <summary>
    /// Navigate to Afkapwaardes
    /// </summary>
    public class AfkapwaardesCommand: NavigationCommand
	{
		public Page Execute (MainViewModel viewModel)
		{
			return new Afkapwaardes (viewModel);
		}
	}

    /// <summary>
    /// Navigate to OverApp
    /// </summary>
    public class OverAppCommand: NavigationCommand
	{
		public Page Execute (MainViewModel viewModel)
		{
			return new OverApp (viewModel);
		}
	}
	/// <summary>
	/// Navigate to OverApp
	/// </summary>
	public class HeimlichCommand: NavigationCommand
	{
		public Page Execute (MainViewModel viewModel)
		{
			return new Heimlich (viewModel);
		}
	}

}



