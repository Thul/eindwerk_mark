﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Xamarin.Forms;


namespace Eindwerk.Droid
{
	[Activity (Label = "Eindwerk", Icon="@android:color/transparent", ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
	public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsApplicationActivity
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);


			global::Xamarin.Forms.Forms.Init (this, bundle);
			this.Window.SetFlags(WindowManagerFlags.KeepScreenOn, WindowManagerFlags.KeepScreenOn);
			App.ScreenSize = new Size( Resources.DisplayMetrics.WidthPixels / Resources.DisplayMetrics.Density, Resources.DisplayMetrics.HeightPixels/Resources.DisplayMetrics.Density);
			LoadApplication (new Eindwerk.App ());
		}
	}
}
