﻿using System;
using Xamarin.Forms;
using SimpleAudioForms.Droid;
using Android.Media;
using Eindwerk.Business.Logic;


[assembly: Dependency(typeof(AudioService))]

namespace SimpleAudioForms.Droid
{
	public class AudioService : IAudio
	{
		private static bool  _isPlaying = false;
		private static MediaPlayer _mediaPlayer;

		public AudioService() {
			_isPlaying = false;
		}

		public void PlayMp3File(string fileName)
		{
			if (!_isPlaying)
			{
				_isPlaying = true;
				if (_mediaPlayer == null) _mediaPlayer = new MediaPlayer ();
				_mediaPlayer.Reset ();

				var fd = Xamarin.Forms.Forms.Context.Assets.OpenFd(fileName);
				_mediaPlayer.SetDataSource(fd.FileDescriptor,fd.StartOffset,fd.Length);
				_mediaPlayer.Looping = true;
				_mediaPlayer.Prepare();
				_mediaPlayer.Start ();
			}

		}

		public void StopPlayingFile()
		{
			if (_isPlaying) 
			{
				_isPlaying = false;
				_mediaPlayer.Stop ();
				_mediaPlayer.Release ();
				_mediaPlayer = null;
			}
		}
	}
}
