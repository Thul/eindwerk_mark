﻿using Eindwerk.Business.Logic;
using Foundation;
using System;
using System.Collections.Generic;
using System.Text;
using UIKit;

namespace Eindwerk.iOS.Service
{
    public class PhoneDialerIOS : IDialer
    {
        public bool Dial(string number)
        {
            return UIApplication.SharedApplication.OpenUrl(
                new NSUrl("tel:" + number));
        }
    }
}
