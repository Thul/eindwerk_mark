﻿using System;
using AVFoundation;
using AudioToolbox;
using Foundation;
using UIKit;
using DependencyServiceSample.iOS;
using Eindwerk.Business.Logic;
[assembly: Xamarin.Forms.Dependency(typeof(AudioService))]
 
namespace DependencyServiceSample.iOS
{
    public class AudioServiceIOS : IAudio
    {
        public AudioServiceIOS() {}
        private NSUrl _songUrl;
		private bool _isPlaying = false;
		private AVAudioPlayer _mediaPlayer

		public void PlayMp3File(string fileName)
		{
			if (!_isPlaying) 
			{
				_songUrl = new NSUrl(fileName);
	            NSError err;
	            _mediaPlayer= new AVAudioPlayer (_songUrl, "mp3", out err);
	            _mediaPlayer.Volume = MusicVolume;
	            _mediaPlayer.FinishedPlaying += delegate { 
	                // backgroundMusic.Dispose(); 
	                backgroundMusic = null;
	            };
	            _mediaPlayer.NumberOfLoops=-1;
            	_mediaPlayer.Play();
			}

		}

		public void StopPlayingFile()
		{
			if (_isPlaying) 
			{
				_mediaPlayer.Stop();
			}
		}
    }
}